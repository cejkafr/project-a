﻿using Game.Entity;
using Unity.Mathematics;

namespace Game.Inventory
{
    public class InventoryItem
    {
        public readonly IItem Item;

        public IInventory Inventory { get; }
        public int2 Position => BoundingBox.Position;
        public InventoryRotation Rotation { get; }

        public BoundingBox BoundingBox { get; }
        public int2 Size => Item.Size;
        public int Weight => Item.Weight;

        public InventoryItem(IInventory inventory, IItem item, int2 position, InventoryRotation rotation)
        {
            Inventory = inventory;
            Item = item;
            Rotation = rotation;
            BoundingBox = new BoundingBox(position,
                rotation == InventoryRotation.Horizontal ? Size : new int2(Size.y, Size.x));
        }
    }
}