﻿using System.Collections.Generic;
using Databases;
using Game.Entity;
using Unity.Mathematics;

namespace Game.Inventory
{
    public delegate void EquipmentEvent(IEquipment equipment, InventoryAction action, int2 position, IItem entity);
    
    public interface IEquipment
    {
        event EquipmentEvent OnChange;
        
        int OwnerId { get; }
        List<EquipmentSection> Sections { get; }    

        bool Equip(IItem item);
        bool Equip(IItem item, int2 position);
        bool UnEquip(int2 position);

        T FindBySlotType<T>(EquipmentSlotType type) where T : IItem;
        // T FindAndTakeByType<T>(EquipmentSlotType type) where T : IItem;
        T FindAmmo<T>(AmmoEntry entry) where T : IItem;
        bool Find(IItem item);
    }

    public class EquipmentSection
    {
        private readonly EquipmentSectionSettings settings;

        public int Index { get; }
        public string Title => settings.title;
        public List<EquipmentSectionSlot> Slots { get; private set; }

        public EquipmentSection(IEquipment equipment, int index, EquipmentSectionSettings settings)
        {
            this.Index = index;
            this.settings = settings;
            Slots = new List<EquipmentSectionSlot>(settings.slots.Length);
            for (var i = 0; i < settings.slots.Length; i++)
            {
                var slotSettings = settings.slots[i];
                for (var j = 0; j < slotSettings.amount; j++)
                {
                    Slots.Add(new EquipmentSectionSlot(equipment, new int2(index, i + j), slotSettings.size, slotSettings.type, slotSettings.itemType));
                }
            }
        }
    }

    public class EquipmentSectionSlot
    {
        public int2 Position { get; }
        public IItem Item { get; private set; }
        public bool IsEmpty => Item == null;
        public int2 Size { get; }

        public EquipmentSlotType Type { get; }
        public ItemType ItemType { get; }
        
        public IEquipment Equipment { get; }
        
        public EquipmentSectionSlot(IEquipment equipment, int2 position, int2 size, EquipmentSlotType type, ItemType itemType)
        {
            Equipment = equipment;
            Position = position;
            Size = size;
            Type = type;
            ItemType = itemType;
        }

        public void Set(IItem item)
        {
            Item = item;
        }

        public void Clear()
        {
            Item = null;
        }

        public bool IsAcceptable(IItem item)
        {
            return IsEmpty &&
                   ItemType == item.Type && 
                   DoesFitIn(item.Size, Size);
        }
        
        private static bool DoesFitIn(int2 item, int2 container)
        {
            return item.x <= container.x && item.y <= container.y;
        }
    }

}