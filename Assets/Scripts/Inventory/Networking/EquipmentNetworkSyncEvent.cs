﻿using Game.Inventory;

namespace Game.Networking
{
    public readonly struct EquipmentNetworkSyncEvent : INetworkEvent
    {
        public static byte TypeCode => (byte) NetworkEventType.SyncEquipment;

        public byte Code => TypeCode;

        public readonly InventoryAction Action;
        public readonly int EntityId;
        public readonly byte SectionIndex;
        public readonly byte SlotIndex;
        public readonly int ItemId;

        public EquipmentNetworkSyncEvent(object data)
        {
            var arr = (object[]) data;
            Action = (InventoryAction) arr[0];
            EntityId = (int) arr[1];
            SectionIndex = (byte) arr[2];
            SlotIndex = (byte) arr[3];
            ItemId = (int) arr[4];
        }

        public EquipmentNetworkSyncEvent(InventoryAction action, int entityId, int sectionIndex, int slotIndex, int itemId)
        {
            Action = action;
            EntityId = entityId;
            SectionIndex = (byte) sectionIndex;
            SlotIndex = (byte) slotIndex;
            ItemId = itemId;
        }

        public object Serialize()
        {
            return new object[] {Action, EntityId, SectionIndex, SlotIndex, ItemId};
        }

        public override string ToString()
        {
            return $"{nameof(Action)}: {Action}, {nameof(EntityId)}: {EntityId}, {nameof(SectionIndex)}: {SectionIndex}, {nameof(SlotIndex)}: {SlotIndex}, {nameof(ItemId)}: {ItemId}, {nameof(Code)}: {Code}";
        }
    }
}