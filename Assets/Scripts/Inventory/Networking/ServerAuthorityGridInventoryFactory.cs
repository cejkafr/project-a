﻿using Game.Entity;
using Game.Inventory;
using Unity.Mathematics;

namespace Game.Networking
{
    public class ServerAuthorityInventoryFactory : IInventoryFactory
    {
        private readonly IEntityManager entityManager;
        private readonly INetworkEventHandler eventHandler;

        public ServerAuthorityInventoryFactory(IEntityManager entityManager, INetworkEventHandler eventHandler)
        {
            this.entityManager = entityManager;
            this.eventHandler = eventHandler;
        }

        public IInventory Create(int entityId, int2 size)
        {
            return new ServerAuthorityGridInventory(entityManager, eventHandler, entityId, size);
        }
    }
}