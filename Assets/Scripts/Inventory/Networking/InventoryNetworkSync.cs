﻿using Game.Entity;
using Game.Inventory;
using UnityEngine;

namespace Game.Networking
{
    public class InventoryNetworkSync
    {
        private readonly IEntityManager entityManager;
        private readonly INetworkEventHandler eventHandler;

        public InventoryNetworkSync(IEntityManager entityManager, INetworkEventHandler eventHandler)
        {
            this.entityManager = entityManager;
            this.eventHandler = eventHandler;
            // this.eventHandler.RegisterListener(InventoryNetworkSyncEvent.TypeCode, RemoteChange);
        }

        public void RemoteChange(object data)
        {
            var ev = new InventoryNetworkSyncEvent(data);
            Debug.Log("Networking :: Sync :: Inventory :: Received inventory change " + ev);

            var entity = entityManager.GetById<IInventoryEntity>(ev.EntityId);
            if (ev.Action == InventoryAction.Insert)
            {
                var item = entityManager.GetById<IItem>(ev.ItemId);
                if (!entity.Inventory.Insert(item, ev.Position, ev.Rotation, out _))
                {
                    Debug.LogError("Networking :: Sync :: Inventory :: DESYNC item could not be inserted " + ev);
                }
            } else if (ev.Action == InventoryAction.Remove)
            {
                var item = entity.Inventory.Remove(ev.Position);
                if (item == null)
                {
                    Debug.LogError("DESYNC: item could not be removed " + ev);
                    return;
                }
            }
        }

        public void LocalChange(IInventory inventory, InventoryAction action, InventoryItem invItem)
        {
            var ev = new InventoryNetworkSyncEvent(action, false, inventory.OwnerId, invItem.Item.Id, invItem.Position, invItem.Rotation);
            Debug.Log("Networking :: Sync :: Inventory :: Sending inventory change " + ev);
            eventHandler.SendBuffered(ev);
        }
    }
}