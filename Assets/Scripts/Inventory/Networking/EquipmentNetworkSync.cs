﻿using Game.Entity;
using Game.Inventory;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Networking
{
    public class EquipmentNetworkSync
    {
        private readonly IEntityManager entityManager;
        private readonly INetworkEventHandler eventHandler;
        
        public EquipmentNetworkSync(IEntityManager entityManager, INetworkEventHandler eventHandler)
        {
            this.entityManager = entityManager;
            this.eventHandler = eventHandler;
            this.eventHandler.RegisterListener(EquipmentNetworkSyncEvent.TypeCode, RemoteChange);
        }

        private void RemoteChange(object data)
        {
            var ev = new EquipmentNetworkSyncEvent(data);
            Debug.Log("Networking :: Sync :: Equipment :: " + ev);

            var entity = entityManager.GetById<IEquipmentEntity>(ev.EntityId);
            if (ev.Action == InventoryAction.Remove)
            {
                entity.Equipment.UnEquip(new int2(ev.SectionIndex, ev.SlotIndex));
            }
            else
            {
                var item = entityManager.GetById<IItem>(ev.ItemId);
                if (!entity.Equipment.Equip(item, new int2(ev.SectionIndex, ev.SlotIndex)))
                {
                    Debug.LogError("Networking :: Sync :: Equipment :: DESYNC item could not be equipped " + ev);
                }
            }
        }
        
        public void LocalChange(IEquipment equipment, InventoryAction action, int2 position, IItem item)
        {
            var ev = new EquipmentNetworkSyncEvent(action, equipment.OwnerId, position.x, position.y, item?.Id ?? -1);
            Debug.Log("Networking :: Sync :: Equipment :: Sending equipment change " + ev);
            eventHandler.SendBuffered(ev);
            
        }
    }
}