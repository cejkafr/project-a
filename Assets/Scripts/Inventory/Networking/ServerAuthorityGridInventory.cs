﻿using Game.Entity;
using Game.Inventory;
using Photon.Pun;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Networking
{
    public class ServerAuthorityGridInventory : GridInventory
    {
        private readonly IEntityManager entityManager;
        private readonly INetworkEventHandler eventHandler;
        
        public ServerAuthorityGridInventory(IEntityManager entityManager, 
            INetworkEventHandler eventHandler, int ownerId, int2 size) : base(ownerId, size)
        {
            this.entityManager = entityManager;
            this.eventHandler = eventHandler;
            
            this.eventHandler.RegisterListener(InventoryNetworkSyncEvent.TypeCode, IncomingEventHandler);
        }

        public override bool Insert(IItem item, int2 position, InventoryRotation rotation, out InventoryItem invItem)
        {
            if (!CanInsert(item, position, rotation))
            {
                invItem = null;
                return false;
            }
            if (PhotonNetwork.IsMasterClient)
            {
                if (!base.Insert(item, position, rotation, out invItem))
                {
                    invItem = null;
                    return false;
                }
                SendChangeEvent(InventoryAction.Insert, invItem);
                return true;
            }
            else
            {
                SendChangeRequest(InventoryAction.Insert, new InventoryItem(this, item, position, rotation));
                invItem = null;
                return true;
            }
        }

        public override bool Remove(InventoryItem invItem)
        {
            if (!CanRemove(invItem))
            {
                return false;
            }
            if (PhotonNetwork.IsMasterClient)
            {
                if (!base.Remove(invItem))
                {
                    return false;
                }
                SendChangeEvent(InventoryAction.Remove, invItem);
                return true;
            }
            else
            {
                SendChangeRequest(InventoryAction.Remove, invItem);
                return true;
            }
        }

        private void SendChangeRequest(InventoryAction action, InventoryItem invItem)
        {
            var ev = new InventoryNetworkSyncEvent(action, true, OwnerId, invItem.Item.Id, invItem.Position, invItem.Rotation);
            Debug.Log("Networking :: Sync :: Inventory :: Sending inventory change REQUEST " + ev);
            eventHandler.SendToMaster(ev);            
        }
        
        private void SendChangeEvent(InventoryAction action, InventoryItem invItem)
        {
            var ev = new InventoryNetworkSyncEvent(action, false, OwnerId, invItem.Item.Id, invItem.Position, invItem.Rotation);
            Debug.Log("Networking :: Sync :: Inventory :: Sending inventory change " + ev);
            eventHandler.SendBuffered(ev);
        }

        private void IncomingEventHandler(object data)
        {
            var ev = new InventoryNetworkSyncEvent(data);
            if (ev.EntityId != OwnerId)
            {
                return;
            }

            if (ev.Request)
            {
                HandleChangeRequest(ev);
            }
            else
            {
                HandleChangeEvent(ev);
            }
        }
        
        private void HandleChangeEvent(InventoryNetworkSyncEvent ev)
        {
            Debug.Log("Networking :: Sync :: Inventory :: Received inventory change " + ev);

            if (ev.Action == InventoryAction.Insert)
            {
                var item = entityManager.GetById<IItem>(ev.ItemId);
                if (!base.Insert(item, ev.Position, ev.Rotation, out _))
                {
                    Debug.LogError("Networking :: Sync :: Inventory :: DESYNC item could not be inserted " + ev);
                }
            } else if (ev.Action == InventoryAction.Remove)
            {
                var invItem = FindBy(ev.Position);
                if (invItem == null)
                {
                    Debug.LogError("DESYNC: item could not be removed " + ev);
                    return;
                }
                base.Remove(invItem);
            }
        }

        private void HandleChangeRequest(InventoryNetworkSyncEvent ev)
        {
            Debug.Log("Networking :: Sync :: Inventory :: Received inventory change REQUEST " + ev);
            if (ev.Action == InventoryAction.Insert)
            {
                var item = entityManager.GetById<IItem>(ev.ItemId);
                Insert(item, ev.Position, ev.Rotation, out _);
            }
            else
            {
                var invItem = FindBy(ev.Position);
                Remove(invItem);
            }
        }
    }
}