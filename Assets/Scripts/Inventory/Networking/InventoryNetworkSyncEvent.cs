﻿using Game.Inventory;
using Unity.Mathematics;

namespace Game.Networking
{
    public readonly struct InventoryNetworkSyncEvent : INetworkEvent
    {
        public static byte TypeCode => (byte) NetworkEventType.SyncInventory;

        public byte Code => TypeCode;

        public readonly InventoryAction Action;
        public readonly bool Request;
        public readonly int EntityId;
        public readonly int ItemId;
        public readonly int2 Position;
        public readonly InventoryRotation Rotation;

        public InventoryNetworkSyncEvent(object data)
        {
            var arr = (object[]) data;
            Action = (InventoryAction) arr[0];
            Request = (bool) arr[1];
            EntityId = (int) arr[2];
            ItemId = (int) arr[3];
            Position = new int2((int) arr[4], (int) arr[5]);
            Rotation = (InventoryRotation) arr[6];
        }

        public InventoryNetworkSyncEvent(InventoryAction action, bool request, int entityId, int itemId, int2 position,
            InventoryRotation rotation)
        {
            Action = action;
            Request = request;
            EntityId = entityId;
            ItemId = itemId;
            Position = position;
            Rotation = rotation;
        }

        public object Serialize()
        {
            return new object[] {Action, Request, EntityId, ItemId, Position.x, Position.y, Rotation};
        }

        public override string ToString()
        {
            return $"{nameof(Action)}: {Action}, {nameof(Request)}: {Request}, {nameof(EntityId)}: {EntityId}, {nameof(ItemId)}: {ItemId}, {nameof(Position)}: {Position}, {nameof(Rotation)}: {Rotation}, {nameof(Code)}: {Code}";
        }
    }
}