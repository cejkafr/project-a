﻿using System.Collections.Generic;
using Game.Entity;
using Game.Inventory;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Inventory.UI
{
    public class EquipmentUI : MonoBehaviour
    {
        public GameObject sectionPrefab;

        private List<EquipmentSectionUI> sections = new List<EquipmentSectionUI>();

        private IEquipment equipment;
        
        public void BindTo(IEquipment equipment)
        {
            this.equipment = equipment;
            this.equipment.OnChange += ChangeHandler;
            InitEntries();
            Refresh();
        }

        private void ChangeHandler(IEquipment _, InventoryAction action, int2 position, IItem item)
        {
            Refresh();
        }
        
        private void InitEntries()
        {
            foreach (var section in equipment.Sections)
            {
                var inst = Instantiate(sectionPrefab, transform);
                var entry = inst.GetComponent<EquipmentSectionUI>();
                entry.Init(section.Title, section.Slots);
                sections.Add(entry);
            }
        }
        
        public void Refresh()
        {
            foreach (var section in sections)
            {
                section.Refresh();
            }
        }
    }
}
