﻿using System;
using Core.Frontend;
using Frontend.UI;
using Game.Entity;
using Pools;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Inventory.UI
{
    public class InventoryItemUI : MonoBehaviour, IDraggable, IPointerDownHandler, IPooledObject
    {
        [SerializeField] private Image background;
        [SerializeField] private Image icon;

        public int slotSize;
        public int slotSpacing;

        public GameObject ArchType { get; set; }

        public InventoryItem InvItem { get; private set; }

        private RectTransform rectTransform;

        public IItem Item => InvItem.Item;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        public T Recycle<T>(Vector3 pos, Quaternion rot) where T : IPooledObject
        {
            throw new NotImplementedException();
        }

        public T Recycle<T>(Transform parent) where T : IPooledObject
        {
            transform.SetParent(parent);
            return (T) (IPooledObject) this;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            InvItem = null;
        }

        public void Show(InventoryItem invItem)
        {
            InvItem = invItem;
            icon.sprite = invItem.Item.Icon;
            rectTransform.sizeDelta = new Vector2(
                slotSize * invItem.Size.x + slotSpacing * invItem.Size.x - 1, 
                slotSize * invItem.Size.y + slotSpacing * invItem.Size.y - 1);
            rectTransform.localScale = Vector3.one;
            gameObject.SetActive(true);

            var trigger = GetComponent<TooltipTrigger>();
            trigger.header = invItem.Item.Title;
            trigger.content = invItem.Item.Description;
        }

        public void SetPosition(Vector2 position)
        {
            rectTransform.localPosition = position;

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (InvItem.Item == null)
            {
                return;
            }

            var offset = rectTransform.InverseTransformPoint(eventData.position);
            DragAndDropSystem.Drag(this, offset);
        }

        public void OnDragStart()
        {

        }

        public void OnDragCancel()
        {

        }

        public void OnDragSuccess(IDroppable droppable)
        {
            InvItem.Inventory.Remove(InvItem);
        }
        
    }
}