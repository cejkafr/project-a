﻿using System.Collections.Generic;
using Game.Inventory;
using TMPro;
using UnityEngine;

namespace Game.Inventory.UI
{
    public class EquipmentSectionUI : MonoBehaviour
    {
        [SerializeField] private GameObject slotPrefab;
        
        [SerializeField] private TextMeshProUGUI header;
        [SerializeField] private Transform content;

        public readonly List<EquipmentSlotUI> slots = new List<EquipmentSlotUI>();

        public void Init(string headerText, List<EquipmentSectionSlot> slots)
        {
            header.text = headerText;
            foreach (var slot in slots)
            {
                var inst = Instantiate(slotPrefab, content);
                var slotUi = inst.GetComponent<EquipmentSlotUI>();
                this.slots.Add(slotUi);
                slotUi.Init(slot);
            }
        }

        public void Refresh()
        {
            foreach (var slotUI in slots)
            {
                slotUI.Refresh();   
            }
        }
    }
}