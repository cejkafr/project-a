﻿using System.Collections.Generic;
using Extensions;
using Frontend.UI;
using Pools;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventory.UI
{
    public class GridInventoryUI : MonoBehaviour, IDroppable
    {
        [SerializeField] private GameObject slotPrefab;
        [SerializeField] private GameObject itemPrefab;
        [SerializeField] private GridLayoutGroup gridLayout;
        [SerializeField] private int slotSize;
        [SerializeField] private int slotSpacing;

        private UnmanagedObjectPool pool;
        private RectTransform rectTransform;
        private IInventory inventory;

        private readonly List<InventorySlotUI> slots = new List<InventorySlotUI>();
        private readonly List<InventoryItemUI> inventoryItems = new List<InventoryItemUI>();

        private void Awake()
        {
            pool = FindObjectOfType<UnmanagedObjectPool>();
            rectTransform = GetComponent<RectTransform>();
        }

        public void BindTo(IInventory inventory)
        {
            if (this.inventory != null)
            {
                this.inventory.OnChange -= ChangeHandler;
            }
            this.inventory = inventory;
            this.inventory.OnChange += ChangeHandler;
            InitSlots();
            Refresh();
        }

        private void ChangeHandler(IInventory _, InventoryAction action, InventoryItem invItem)
        {
            if (action == InventoryAction.Remove)
            {
                var invItemUi = FindItemBySlot(invItem);
                if (invItemUi == null)
                {
                    Debug.LogError("Failed to find inventoryItemUI, refreshing inventory UI.");
                    Refresh();
                }
                else
                {
                    ReturnToUnused(invItemUi);
                }
            }
            else
            {
                InitOrCreateUnusedItem(invItem);
            }
        }

        private InventoryItemUI FindItemBySlot(InventoryItem invItem)
        {
            return inventoryItems.Find(invItemUi => invItem.Equals(invItemUi.InvItem));
        }

        private InventoryItemUI InitOrCreateUnusedItem(InventoryItem invItem)
        {
            var invItemUi = pool.CreateFromPool<InventoryItemUI>(itemPrefab, rectTransform);
            inventoryItems.Add(invItemUi);
            invItemUi.SetPosition(InventoryToGridPos(invItem.Position));
            invItemUi.Show(invItem);
            return invItemUi;
        }

        private void ReturnToUnused(InventoryItemUI invItemUi)
        {
            invItemUi.Hide();
            pool.ReturnToPool(invItemUi);
            inventoryItems.Remove(invItemUi);
        }

        private void InitSlots()
        {
            gridLayout.constraintCount = inventory.Size.x;
            var slotCount = inventory.Size.x * inventory.Size.y;
            if (slots.Count != slotCount)
            {
                for (var i = 0; i < slotCount; i++)
                {
                    var inst = pool.CreateFromPool<InventorySlotUI>(slotPrefab, gridLayout.transform);
                    
                    inst.Hide(false);
                    slots.Add(inst);
                }
            }
            var size = new Vector2(
                rectTransform.sizeDelta.x,
                inventory.Size.y * slotSize + (inventory.Size.y - 1 * slotSpacing)
            );
            rectTransform.sizeDelta = size;
            
        }

        public void Refresh()
        {
            if (!inventoryItems.IsEmpty())
            {
                var copy = inventoryItems.ToArray();
                foreach (var invItemUi in copy)
                {
                    ReturnToUnused(invItemUi);
                }
            }
            foreach (var t in inventory.Items)
            {
                InitOrCreateUnusedItem(t);
            }
        }

        public void OnDragOver(IDraggable draggable, Vector2 position)
        {
        }

        public void OnDragOut(IDraggable draggable, Vector2 position)
        {
        }

        public void OnDragStays(IDraggable draggable, Vector2 position)
        {
        }

        public bool OnDragDrop(IDraggable draggable, Vector2 position)
        {
            var invPos = GridToInventoryPos(rectTransform.InverseTransformPoint(position));
            return inventory.Insert(draggable.Item, invPos, InventoryRotation.Horizontal, out _);
        }

        private Vector2 InventoryToGridPos(int2 position)
        {
            return new Vector2(
                slotSize * position.x + slotSpacing * position.x,
                (slotSize * position.y + slotSpacing * position.y) * -1);
        }

        private int2 GridToInventoryPos(Vector2 position)
        {
            return new int2(
                Mathf.FloorToInt(position.x / slotSize + slotSpacing),
                Mathf.FloorToInt(Mathf.Abs(position.y) / slotSize + slotSpacing)) - 1;
        }
    }
}