﻿using Core.Frontend;
using Frontend.UI;
using Game.Entity;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Inventory.UI
{
    public class EquipmentSlotUI : MonoBehaviour, IDraggable, IDroppable, IPointerDownHandler
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Image item;
        [SerializeField] private int2 slotSize;
        [SerializeField] private int2 slotSpacing;
        private EquipmentSectionSlot slot;

        public IItem Item => slot.Item;

        public void Init(EquipmentSectionSlot slot)
        {
            this.slot = slot;
            rectTransform.sizeDelta = new Vector2(
                slotSize.x * slot.Size.x + slotSpacing.x * slot.Size.x - 1, 
                slotSize.y * slot.Size.y + slotSpacing.y * slot.Size.y - 1);
            rectTransform.localScale = Vector3.one;
            Refresh();
        }

        public void Refresh()
        {
            if (slot.IsEmpty)
            {
                var trigger = GetComponent<TooltipTrigger>();
                trigger.header = "";
                trigger.content = "";
                item.gameObject.SetActive(false);
            }
            else
            {
                item.sprite = slot.Item.Icon;
                item.gameObject.SetActive(true);
                item.GetComponent<RectTransform>()
                    .sizeDelta = new Vector2(
                    slotSize.x * slot.Item.Size.x + slotSpacing.x * slot.Item.Size.x - 1, 
                    slotSize.y * slot.Item.Size.y + slotSpacing.y * slot.Item.Size.y - 1
                );
                var trigger = GetComponent<TooltipTrigger>();
                trigger.header = Item.Title;
                trigger.content = Item.Description;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (slot.IsEmpty)
            {
                return;
            }
            var offset = rectTransform.InverseTransformPoint(eventData.position);
            DragAndDropSystem.Drag(this, offset);
        }

        public void OnDragStart()
        {
        }

        public void OnDragCancel()
        {
            
        }

        public void OnDragSuccess(IDroppable droppable)
        {
            slot.Equipment.UnEquip(slot.Position);
        }
        
        public void OnDragOver(IDraggable draggable, Vector2 position)
        {
        }

        public void OnDragOut(IDraggable draggable, Vector2 position)
        {
        }

        public void OnDragStays(IDraggable draggable, Vector2 position)
        {
        }

        public bool OnDragDrop(IDraggable draggable, Vector2 position)
        {
            return slot.Equipment.Equip(draggable.Item, slot.Position);
        }
    }
}