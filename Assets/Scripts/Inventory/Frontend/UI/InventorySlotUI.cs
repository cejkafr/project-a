﻿using Pools;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventory.UI
{
    public class InventorySlotUI : MonoBehaviour, IPooledObject
    {
        [SerializeField] private Image background;
        
        public Color enabledColor;
        public Color disabledColor;
        
        public bool isEnabled;

        public GameObject ArchType { get; set; }

        public T Recycle<T>(Vector3 pos, Quaternion rot) where T : IPooledObject
        {
            throw new System.NotImplementedException();
        }

        public T Recycle<T>(Transform parent) where T : IPooledObject
        {
            transform.SetParent(parent);
            return (T) (IPooledObject) this;
        }

        public void Hide(bool value)
        {
            transform.localScale = Vector3.one;
            gameObject.SetActive(!value);
        }
        
        public void SetEnabled(bool value)
        {
            isEnabled = value;
            background.color = isEnabled ? enabledColor : disabledColor;
        }

        public void Clear()
        {
            
        }
    }
}