﻿using Unity.Mathematics;

namespace Game.Inventory
{
    public class GridInventoryFactory : IInventoryFactory
    {
        public IInventory Create(int entityId, int2 size)
        {
            return new GridInventory(entityId, size);
        }
    }
}