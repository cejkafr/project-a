﻿using System.Collections.Generic;
using System.Linq;
using Game.Entity;
using Unity.Mathematics;

namespace Game.Inventory
{
    public class GridInventory : IInventory
    {
        public event InventoryEvent OnChange;

        public int OwnerId { get; }
        public int2 Size => BoundingBox.Size;
        public List<InventoryItem> Items { get; }

        private BoundingBox BoundingBox { get; }
        
        public GridInventory(int ownerId, int2 size)
        {
            OwnerId = ownerId;
            BoundingBox = new BoundingBox(int2.zero, size);
            Items = new List<InventoryItem>();
        }

        public bool Insert(IItem item)
        {
            return CanInsert(item, out var position, out var rotation)
                   && Insert(item, position, rotation, out _);
        }

        public virtual bool Insert(IItem item, int2 position, InventoryRotation rotation, out InventoryItem invItem)
        {
            if (!CanInsert(item, position, rotation))
            {
                invItem = null;
                return false;
            }

            invItem = new InventoryItem(this, item, position, rotation);
            Items.Add(invItem);
            OnChange?.Invoke(this, InventoryAction.Insert, invItem);

            return true;
        }

        public bool Remove(IItem item)
        {
            var invItem = FindBy(item);
            return Remove(invItem);
        }

        public IItem Remove(int2 position)
        {
            var invItem = FindBy(position);
            return Remove(invItem) ? invItem.Item : null;
        }

        public virtual bool Remove(InventoryItem invItem)
        {
            if (!CanRemove(invItem))
            {
                return false;
            }

            Items.Remove(invItem);
            OnChange?.Invoke(this, InventoryAction.Remove, invItem);

            return true;
        }

        public bool CanRemove(InventoryItem invItem)
        {
            if (invItem == null)
            {
                return false;
            }

            return true;
        }
        
        public bool CanInsert(IItem item)
        {
            return CanInsert(item, out _, out _);
        }

        public bool CanInsert(IItem item, out int2 position, out InventoryRotation rotation)
        {
            rotation = InventoryRotation.Horizontal;
            var max = Size - item.Size;
            for (var y = 0; y < max.y; y++)
            {
                for (var x = 0; x < max.x; x++)
                {
                    position = new int2(x, y);
                    if (CanInsert(item, position, rotation))
                    {
                        return true;
                    }
                }
            }

            position = int2.zero;
            return false;
        }

        public bool CanInsert(IItem item, int2 position, InventoryRotation rotation)
        {
            var boundingBox = new BoundingBox(position,
                rotation == InventoryRotation.Horizontal ? item.Size : new int2(item.Size.y, item.Size.x));
            if (!BoundingBox.ContainsCheck(boundingBox))
            {
                return false;
            }

            foreach (var invItem in Items)
            {
                if (invItem.Item.Equals(item)) continue;
                if (invItem.Position.Equals(position) || 
                    invItem.BoundingBox.CollisionCheck(boundingBox))
                {
                    return false;
                }
            }

            return true;
        }

        public int TotalWeight()
        {
            return Items.Sum(item => item.Weight);
        }

        protected InventoryItem FindBy(IItem item)
        {
            foreach (var invItem in Items)
            {
                if (invItem.Item.Equals(item))
                {
                    return invItem;
                }
            }

            return null;
        }

        protected InventoryItem FindBy(int2 position)
        {
            foreach (var invItem in Items)
            {
                if (invItem.Position.Equals(position))
                {
                    return invItem;
                }
            }

            return null;
        }
    }
}