﻿using Game.Entity;

namespace Game.Inventory
{
    public interface IEquipmentEntity : IEntity
    {
        IEquipment Equipment { get; }
    }
}