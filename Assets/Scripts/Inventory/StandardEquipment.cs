﻿using System.Collections.Generic;
using Databases;
using Game.Entity;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Inventory
{
    public class StandardEquipment : IEquipment
    {
        public event EquipmentEvent OnChange;

        public int OwnerId { get; }
        public List<EquipmentSection> Sections { get; }

        public StandardEquipment(int ownerId, IList<EquipmentSectionSettings> settings)
        {
            OwnerId = ownerId;

            Sections = new List<EquipmentSection>(settings.Count);
            for (var i = 0; i < settings.Count; i++)
            {
                Sections.Add(new EquipmentSection(this, i, settings[i]));
            }
        }

        public bool Equip(IItem item)
        {
            for (var i = Sections.Count - 1; i >= 0; i--)
            {
                var section = Sections[i];
                foreach (var slot in section.Slots)
                {
                    if (Equip(item, new int2(i, slot.Position.y)))
                    {
                        return true;
                    }
                }
            }

            Debug.LogError("Failed to equip into slot");

            return false;
        }

        public bool Equip(IItem item, int2 position)
        {
            var slot = Sections[position.x].Slots[position.y];
            if (slot.Item == item ||
                !slot.IsAcceptable(item))
            {
                return false;
            }

            slot.Set(item);
            OnChange?.Invoke(this, InventoryAction.Insert, position, item);
            return true;
        }

        public bool UnEquip(int2 position)
        {
            var slot = Sections[position.x].Slots[position.y];
            var item = slot.Item;
            slot.Clear();
            OnChange?.Invoke(this, InventoryAction.Remove, position, item);
            return true;
        }

        public T FindBySlotType<T>(EquipmentSlotType type) where T : IItem
        {
            foreach (var section in Sections)
            {
                foreach (var slot in section.Slots)
                {
                    if (slot.Type == type)
                    {
                        return (T) slot.Item;
                    }
                }
            }

            return default;
        }

        public T FindAmmo<T>(AmmoEntry entry) where T : IItem
        {
            foreach (var section in Sections)
            {
                foreach (var slot in section.Slots)
                {
                    if (slot.IsEmpty || slot.Item.Type != ItemType.AmmoBox)
                        continue;
                    // if (slot.Item. == entry.id)
                    // {
                    //     
                    // }
                }
            }

            return default;
        }

        public bool Find(IItem item)
        {
            foreach (var section in Sections)
            {
                foreach (var slot in section.Slots)
                {
                    if (!slot.IsEmpty && slot.Item == item)
                    {
                        return true;
                    }
                }
            }

            return default;
        }
    }
}