﻿using Game.Entity;

namespace Game.Inventory
{
 
    public interface IInventoryEntity : IEntity 
    {
        IInventory Inventory { get; }
    }
    
}