﻿using System.Collections.Generic;
using Game.Entity;
using Unity.Mathematics;

namespace Game.Inventory
{
    public delegate void InventoryEvent(IInventory inventory, InventoryAction type, InventoryItem invItem);

    public interface IInventory
    {
        event InventoryEvent OnChange;

        int OwnerId { get; }
        int2 Size { get; }
        List<InventoryItem> Items { get; }
        
        bool Insert(IItem item);
        bool Insert(IItem item, int2 position, InventoryRotation rotation, out InventoryItem invItem);
        bool Remove(IItem item);
        IItem Remove(int2 position);
        bool Remove(InventoryItem invItem);
        bool CanRemove(InventoryItem invItem);
        bool CanInsert(IItem item);
        bool CanInsert(IItem item, out int2 position, out InventoryRotation rotation);
        bool CanInsert(IItem item, int2 position, InventoryRotation rotation);
        int TotalWeight();
    }

    public enum InventoryRotation : byte
    {
        Horizontal = 0,
        Vertical = 90,
    }

    public enum InventoryAction : byte
    {
        Insert,
        Remove,
    }
}