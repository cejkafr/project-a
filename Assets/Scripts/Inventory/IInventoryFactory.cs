﻿using Unity.Mathematics;

namespace Game.Inventory
{
    public interface IInventoryFactory
    {
        IInventory Create(int entityId, int2 size);
    }
}