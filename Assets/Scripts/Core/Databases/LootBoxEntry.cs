﻿using Unity.Mathematics;
using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/LootBox")]
    public class LootBoxEntry : AEntityEntry
    {
        public string title;
        public Sprite icon;
        public int2 size;
        public GameObject prefab;
    }
}