﻿using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Ammo Box")]
    public class AmmoBoxEntry : AItemEntry
    {
        public AmmoEntry ammo;
        public int amount;
    }
}