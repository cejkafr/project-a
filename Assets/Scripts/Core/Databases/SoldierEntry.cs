﻿using System;
using FMODUnity;
using Game.Entity;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Soldier")]
    public class SoldierEntry : AEntityEntry
    {
        public new string name;
        public string description;
        public Sprite icon;
        public GameObject prefab;
        public MotionEntry motion;
        public int health;
        public int stamina;
        public float walkSpeed;
        public float runSpeed;
        public float sprintSpeed;
        public int rotationSpeed;
        public float aimSpeed;
        public float aimWalkSpeed;
        public int aimRotationSpeed;
        public int2 inventorySize;
        [EventRef] public string meleeSfx;
        [EventRef] public string hurtSfx;
        [EventRef] public string deathSfx;
        public EquipmentSectionSettings[] equipmentSlots;
    }

    [Serializable]
    public struct EquipmentSectionSettings
    {
        public string title;
        public EquipmentSectionSlotSettings[] slots;
    }

    [Serializable]
    public struct EquipmentSectionSlotSettings
    {
        public EquipmentSlotType type;
        public int2 size;
        public int amount;
        public bool requireItemType;
        public ItemType itemType;
    }
}