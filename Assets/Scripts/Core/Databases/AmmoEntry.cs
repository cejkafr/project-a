﻿using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Ammo")]
    public class AmmoEntry : ScriptableObject
    {
        public int id;
        public string title;
        public string description;
        public Sprite icon;
        public GameObject shellPrefab;
    }
}