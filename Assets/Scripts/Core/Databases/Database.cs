﻿using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Database")]
    public class Database : ScriptableObject
    {
        public string title;
        public MotionEntry[] motions;
        public DamageTypeEntry[] damageTypes;
        public WeaponEntry[] weapons;
        public AmmoEntry[] ammo;
        public AmmoBoxEntry[] ammoBoxes;
        public ThrowableEntry[] throwables;
        public SoldierEntry[] soldiers;
        public LootBoxEntry[] lootBoxes;
    }
}