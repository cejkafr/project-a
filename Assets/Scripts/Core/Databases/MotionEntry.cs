﻿using System;
using Combat;
using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Motion")]
    public class MotionEntry : ScriptableObject
    {
        public int id;
        public string title;
        public float moveSpeedMult = 1;
        public float rotationSpeedMult = 1;
        public float equipShowDelay;
        public float equipTimeTotal;
        public float unEquipHideDelay;
        public float unEquipTimeTotal;
        public float equipUnEquipSpeed = 1;
        public float reloadTime;
        public MeleeAttack[] meleeAttacks;
    }
    
    [Serializable]
    public class MeleeAttack
    {
        public enum Type
        {
            Normal, Hard,
        }
        
        public int animationId;
        public Type type;
        public bool stationary;
        public float length;
        public float applyDamageDelay;
        public AnimationCurve movementCurve;
        public Vector3 raycastOffset;
        public float rbForce;
        public Damage damage;
    }
    
}