﻿using Unity.Mathematics;
using UnityEngine;

namespace Databases
{
    public abstract class AItemEntry : AEntityEntry
    {
        public string title;
        public string description;
        public Sprite icon;
        public int2 size;
        public int weight;
        public GameObject prefab;
    }
}