﻿using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Damage Type")]
    public class DamageTypeEntry : ScriptableObject
    {
        public int id;
        public string title;
        public string description;
        public Sprite icon;
    }
}