﻿using Combat;
using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Weapon")]
    public class WeaponEntry : AItemEntry
    {
        public MotionEntry motion;
        public AmmoEntry ammo;
        public ItemBodySlotType bodySlot;
        public EquipmentSlotType equipmentSlot;
        public float rbForce;
        public bool raycast;
        public float bulletSpeed;
        public int maxRange;
        public int magazineSize;
        public float reloadTimeMult = 1;
        public bool perBulletReload;
        public float fireSpeed;
        public int pelletsPerShot = 1;
        public Damage damage;
        public bool singleMode;
        public bool semiAutoMode;
        public int semiAutoShotCount;
        public bool fullAutoMode;
    }
}