﻿using UnityEngine;

namespace Databases
{
    [CreateAssetMenu(menuName = "Databases/Throwable")]
    public class ThrowableEntry : AItemEntry
    {
        public float triggerTime;
    }
}