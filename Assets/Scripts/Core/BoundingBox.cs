﻿using Unity.Mathematics;

public class BoundingBox
{
    /// <summary>
    ///     Gets or Sets the xy-coordinate top-left position
    ///     of the bounding box
    /// </summary>
    public int2 Position { get; set; }

    /// <summary>
    ///     The size of the bounding box
    /// </summary>
    public int2 Size { get; set; }
    
    /// <summary>
    ///     Gets or Sets the y-coordinate position of the top
    ///     edge of the bounding box
    /// </summary>
    public float Top => Position.y;

    /// <summary>
    ///     Gets or Sets the y-coordinate position of the bottom
    ///     edge of the bounding box
    /// </summary>
    public float Bottom => Position.y + Size.y;

    /// <summary>
    ///     Gets or Sets the x-coordinate position of the left
    ///     edge of the bounding box
    /// </summary>
    public float Left => Position.x;

    /// <summary>
    ///     Gets or Sets the x-coordinate position of the right
    ///     edge of the bounding box
    /// </summary>
    public float Right => Position.x + Size.x;

    /// <summary>
    ///     Creates a new bounding box instance
    /// </summary>
    /// <param name="position">The position of the bounding box</param>
    /// <param name="size">The size of the bounding box</param>
    public BoundingBox(int2 position, int2 size)
    {
        //  Set the properties
        Position = position;
        Size = size;
    }
    
    /// <summary>
    ///     Performs Axis-Aligned Bounding Box collision check against another
    ///     BoundingBox
    /// </summary>
    /// <param name="other">The other BoundingBox to check if this and that one is colliding</param>
    /// <returns>
    ///     True if they are colliding; otherwise false.
    /// </returns>
    public bool CollisionCheck(BoundingBox other)
    {
        // 1. Is the left edge of this BoundingBox less than the right edge of the other BoundingBox
        // 2. Is the right edge of this BoundingBox greater than the left edge of the other BoundingBox
        // 3. Is the top edge of this BoundingBox less than the bottom edge of the other BoundingBox
        // 4. Is the bottom edge of this BoundingBox greater than the top edge of the other BoundingBox
        return this.Left < other.Right &&
               this.Right > other.Left &&
               this.Top < other.Bottom &&
               this.Bottom > other.Top;
    }

    public bool ContainsCheck(BoundingBox other)
    {
        return other.Left >= this.Left &&
               other.Top >= this.Top &&
               other.Right <= this.Right &&
               other.Bottom <= this.Bottom;
    }
}