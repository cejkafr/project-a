﻿using Databases;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Entity
{
    public enum ItemType
    {
        Weapon, Throwable, AmmoBox,
    }
    
    public interface IItem : IEntity
    {
        string Title { get; }
        string Description { get; }
        ItemType Type { get; }
        Sprite Icon { get; }
        int2 Size { get; }
        int Weight { get; }
        GameObject Prefab { get; }
    }
}