﻿using Databases;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Entity
{
    public abstract class AItem<T> : AEntity<T>, IItem where T : AItemEntry
    {
        public string Title => Entry.title;
        public string Description => Entry.description;
        public ItemType Type { get; }
        public Sprite Icon => Entry.icon;
        public int2 Size => Entry.size;
        public int Weight => Entry.weight;
        public GameObject Prefab => Entry.prefab;

        protected AItem(int id, ItemType type, T entry) : base(id, entry)
        {
            Type = type;
        }
    }
}