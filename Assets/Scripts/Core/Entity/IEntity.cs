﻿using Databases;

namespace Game.Entity
{
    public interface IEntity
    {
        int Id { get; }
        string EntryId { get; }
        PlayerId Owner { get; }
    }

    public abstract class AEntity<T> : IEntity where T : AEntityEntry
    {
        public readonly T Entry;

        public int Id { get; }
        public string EntryId => Entry.id;
        public PlayerId Owner { get; set; }

        protected AEntity(int id, T entry, PlayerId owner = PlayerId.None)
        {
            Entry = entry;
            Id = id;
            Owner = owner;
        }

        protected bool Equals(AEntity<T> other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AEntity<T>) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}