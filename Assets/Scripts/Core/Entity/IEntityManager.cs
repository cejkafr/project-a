﻿using System.Collections.Generic;

namespace Game.Entity
{
    public delegate void EntityManagerEvent(IEntity entity);
    
    public interface IEntityManager
    {
        event EntityManagerEvent OnCreate;
        event EntityManagerEvent OnUpdate;
        event EntityManagerEvent OnRemove;
        
        void Create(IEntity entity, bool invokeEvent = true);
        void Update(IEntity entity, bool invokeEvent = true);
        void Remove(IEntity entity, bool invokeEvent = true);

        void Clear();

        T GetById<T>(int id) where T : IEntity;
        List<IEntity> GetByOwner(PlayerId owner);
        List<T> GetByOwner<T>(PlayerId owner) where T : IEntity;
        T GetFirstByOwner<T>(PlayerId owner) where T : IEntity;
    }
}