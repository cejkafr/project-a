﻿namespace Combat
{
    public interface IHasHealth
    {
        int HealthMax { get; }
        float Health { get; }
        bool IsDead { get; }

        void ApplyDamage(Damage damage);
    }
}