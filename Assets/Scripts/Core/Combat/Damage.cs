﻿using System;
using Databases;

namespace Combat
{
    [Serializable]
    public struct Damage
    {
        public DamageTypeEntry DamageType;
        public float Amount;

        public Damage(DamageTypeEntry damageType, float amount)
        {
            DamageType = damageType;
            Amount = amount;
        }

        public override string ToString()
        {
            return $"{nameof(DamageType)}: {DamageType}, {nameof(Amount)}: {Amount}";
        }
    }
}