﻿using DG.Tweening;
using Frontend.UI;
using UnityEngine;
using UnityEngine.InputSystem.UI;

namespace Core.Frontend
{
    public class TooltipSystem : MonoBehaviour
    {
        public static TooltipSystem Instance;
        private static Tweener tween;

        [SerializeField] private InputSystemUIInputModule inputModule;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Tooltip tooltip;
        [SerializeField] private float fadeTime = 0.1f;

        private void Awake()
        {
            if (Instance != null)
            {
                print("Destroying duplicate TooltipSystem.");
                Destroy(gameObject);                
            }

            Instance = this;
        }

        private void Start()
        {
            Hide();
        }

        public static void Show(string content, string header = "")
        {
            Instance.tooltip.SetText(content, header);
            if (tween != null && !tween.playedOnce)
            {
                tween.Kill();
            }
            tween = Instance.canvasGroup
                .DOFade(1, Instance.fadeTime)
                .OnStart(() =>
                {
                    Instance.tooltip.SetPivotAndPosition(Instance.inputModule.point.action.ReadValue<Vector2>());
                    Instance.gameObject.SetActive(true);
                });
        }

        public static void Hide()
        {
            if (tween != null && !tween.playedOnce)
            {
                tween.Kill();
            }

            tween = Instance.canvasGroup
                .DOFade(0, Instance.fadeTime)
                .OnComplete(() => Instance.gameObject.SetActive(false));
        }
    }
}