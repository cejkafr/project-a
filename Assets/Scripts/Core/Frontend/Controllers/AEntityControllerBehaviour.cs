﻿using Game.Entity;
using Photon.Pun;

namespace Game.Frontend.Controllers
{
    public class AEntityControllerBehaviour<T> : MonoBehaviourPun, IEntityController where T : IEntity
    {
        public IEntity Id { get; protected set; }
        public T Entity { get; protected set; }
    }
}