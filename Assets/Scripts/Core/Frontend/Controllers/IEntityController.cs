﻿using Game.Entity;

namespace Game.Frontend.Controllers
{
    public interface IEntityController
    {
        IEntity Id { get; }
    }
}