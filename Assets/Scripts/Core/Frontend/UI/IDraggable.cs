﻿using Game.Entity;

namespace Frontend.UI
{
    public interface IDraggable
    {
        IItem Item { get; }

        void OnDragStart();
        void OnDragCancel();
        void OnDragSuccess(IDroppable droppable);
    }
}