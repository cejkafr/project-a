﻿using Core.Frontend;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Frontend.UI
{
    public class TooltipTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private const float Delay = 0.5f;
        private static Tween tween;

        public string header;
        [Multiline] public string content;

        public void OnPointerEnter(PointerEventData eventData)
        {
            tween = DOVirtual.DelayedCall(Delay, 
                () => TooltipSystem.Show(content, header));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            tween.Kill();
            TooltipSystem.Hide();
        }
    }
}