using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frontend.UI
{
    public class Tooltip : MonoBehaviour
    {
        [SerializeField] private Image backgroundImage;
        [SerializeField] private TextMeshProUGUI headerField;
        [SerializeField] private TextMeshProUGUI contentField;
        [SerializeField] private LayoutElement layoutElement;
        [SerializeField] private int characterWrapLimit;
        [SerializeField] private Vector2 offset;

        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        public void SetText(string content, string header = "")
        {
            if (string.IsNullOrEmpty(header))
            {
                headerField.gameObject.SetActive(false);
            }
            else
            {
                headerField.gameObject.SetActive(true);
                headerField.text = header;
            }

            contentField.text = content;

            WrapText();
        }

        private void WrapText()
        {
            var headerLength = headerField.text.Length;
            var contentLength = contentField.text.Length;

            layoutElement.enabled = headerLength > characterWrapLimit || contentLength > characterWrapLimit;
        }

        public void SetPivotAndPosition(Vector2 position)
        {
            rectTransform.pivot = new Vector2(
                position.x / Screen.width,
                position.y / Screen.height);
            transform.position = position + offset;
        }
    }
}