﻿using System;
using System.Collections.Generic;
using Extensions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Frontend.UI
{
    public class DragAndDrop : MonoBehaviour
    {
        [SerializeField] private GraphicRaycaster raycaster;
        [SerializeField] private EventSystem eventSystem;
        [SerializeField] private Image background;
        [SerializeField] private Image icon;

        public int slotSize;
        public int slotSpacing;
        public Vector2 offset;

        private RectTransform rectTransform;

        private IDraggable draggable;
        private IDroppable droppable;

        private PointerEventData pointerEventData;
        private readonly List<RaycastResult> results = new List<RaycastResult>();

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        public void FollowPointer(Vector2 pointerPosition)
        {
            rectTransform.position = pointerPosition - offset;
            pointerEventData = new PointerEventData(eventSystem)
                {position = rectTransform.position};
            RaycastDroppable();
        }

        private void RaycastDroppable()
        {
            results.Clear();
            raycaster.Raycast(pointerEventData, results);

            if (results.IsEmpty())
            {
                this.droppable?.OnDragOut(this.draggable, pointerEventData.position);
                this.droppable = null;
                return;
            }

            IDroppable droppable = null;
            foreach (var result in results)
            {
                droppable = result.gameObject.GetComponent<IDroppable>();
                if (droppable != null)
                {
                    if (!droppable.Equals(this.droppable))
                    {
                        this.droppable?.OnDragOut(this.draggable, pointerEventData.position);
                        this.droppable = droppable;
                        this.droppable.OnDragOver(this.draggable, pointerEventData.position);
                        return;
                    }
                    else
                    {
                        this.droppable?.OnDragStays(this.draggable, pointerEventData.position);
                    }

                    break;
                }
            }

            if (droppable == null)
            {
                this.droppable?.OnDragOut(this.draggable, pointerEventData.position);
                this.droppable = null;
            }
        }

        public void Drag(IDraggable draggable, Vector2 offset)
        {
            this.draggable = draggable;
            this.offset = offset;
            rectTransform.sizeDelta = new Vector2(
                slotSize * draggable.Item.Size.x + slotSpacing * draggable.Item.Size.x - 1,
                slotSize * draggable.Item.Size.y + slotSpacing * draggable.Item.Size.y - 1);
            icon.sprite = draggable.Item.Icon;
            this.draggable.OnDragStart();
        }

        public void Cancel()
        {
            if (draggable == null)
            {
                return;
            }

            if (droppable != null && droppable.OnDragDrop(draggable, rectTransform.position))
            {
                draggable.OnDragSuccess(droppable);
            }
            else
            {
                draggable.OnDragCancel();
            }

            draggable = null;
            droppable = null;
        }
    }
}