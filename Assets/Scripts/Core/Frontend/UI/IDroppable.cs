﻿using UnityEngine;

namespace Frontend.UI
{
    public interface IDroppable
    {
        void OnDragOver(IDraggable draggable, Vector2 position);
        void OnDragOut(IDraggable draggable, Vector2 position);
        void OnDragStays(IDraggable draggable, Vector2 position);
        bool OnDragDrop(IDraggable draggable, Vector2 position);
    }
}