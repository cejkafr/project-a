﻿using System;
using Frontend.UI;
using UnityEngine;
using UnityEngine.InputSystem.UI;

namespace Core.Frontend
{
    public class DragAndDropSystem : MonoBehaviour
    {
        public static DragAndDropSystem Instance;

        [SerializeField] private InputSystemUIInputModule inputModule;
        [SerializeField] private DragAndDrop dragAndDrop;

        private void Awake()
        {
            if (Instance != null)
            {
                print("Destroying duplicate TooltipSystem.");
                Destroy(gameObject);                
            }

            Instance = this;
        }

        private void Start()
        {
            Cancel();
        }

        private void LateUpdate()
        {
            dragAndDrop.FollowPointer(inputModule.point.action.ReadValue<Vector2>());

            if (inputModule.leftClick.action.ReadValue<float>() < 0.5f || 
                inputModule.rightClick.action.triggered)
            {
                Cancel();
            }
        }

        public static void Drag(IDraggable draggable, Vector2 offset)
        {
            Instance.dragAndDrop.Drag(draggable, offset);
            Instance.gameObject.SetActive(true);
        }

        public static void Cancel()
        {
            Instance.dragAndDrop.Cancel();
            Instance.gameObject.SetActive(false);
        }
    }
}