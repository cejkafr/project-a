﻿using System;
using UnityEngine;

namespace Core
{
    [Serializable]
    public struct EnvFx
    {
        public string tag;
        public Material material;
        public GameObject impactFx;
        public float concreteWeight;
        public float gravelWeight;
        public float metalWeight;
        public float woodWeight;
        public float brickWeight;
        public float glassWeight;
        public float grassWeight;
        public float waterWeight;
        public float rockWeight;
    }
    
    [CreateAssetMenu(menuName = "Level Env Settings")]
    public class LevelEnvSettings : ScriptableObject
    {
        public EnvFx[] envFx;
    }
}