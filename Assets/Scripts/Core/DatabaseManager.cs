﻿using System;
using System.Linq;
using Databases;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{
    [SerializeField] private Database database;
    
    public MotionEntry GetMotionById(int id)
    {
        return database.motions.First(e => e.id == id);
    }
    
    public DamageTypeEntry GetDamageTypeById(int id)
    {
        return database.damageTypes.First(e => e.id == id);
    }

    public SoldierEntry GetSoldierById(string id)
    {
        return database.soldiers.First(e => e.id == id);
    }

    public WeaponEntry GetWeaponById(string id)
    {
        return database.weapons.First(e => e.id == id);
    }
    
    public AmmoEntry GetAmmoById(int id)
    {
        return database.ammo.First(e => e.id == id);
    }
    
    public AmmoBoxEntry GetAmmoBoxById(string id)
    {
        return database.ammoBoxes.First(e => e.id == id);
    }
    
    public ThrowableEntry GetThrowableById(string id)
    {
        return database.throwables.First(e => e.id == id);
    }
    
    public LootBoxEntry GetLootBoxById(string id)
    {
        return database.lootBoxes.First(e => e.id == id);
    }
}