﻿using System;

namespace Game.Networking
{
    public interface INetworkEventHandler
    {
        void RegisterListener(byte type, Action<object> listener);
        void Send(INetworkEvent ev);
        void Send(INetworkEvent ev, int actorNumber);
        void SendToMaster(INetworkEvent ev);
        void SendBuffered(INetworkEvent ev);
    }
}