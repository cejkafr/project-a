﻿namespace Game.Networking
{
    public interface INetworkEvent
    {
        byte Code { get; }

        object Serialize();
    }
}