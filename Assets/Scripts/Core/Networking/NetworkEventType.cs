﻿namespace Game.Networking
{
    public enum NetworkEventType : byte
    {
        Wildcart,
        SpawnPlayer,
        Spawned,
        SyncState,
        SyncEntity,
        SyncInventory,
        SyncEquipment,
    }
}