﻿using System;
using System.Collections.Generic;
using Pools;
using UnityEngine;

namespace Pools
{
    public class QueueObjectPool : MonoBehaviour, IObjectPool
    {
        [Serializable]
        public struct PoolSetting
        {
            public GameObject prefab;
            public int size;
            public bool resizeable;
        }

        private readonly struct Pool
        {
            public readonly Queue<GameObject> Queue;
            public readonly bool Resizeable;

            public Pool(int initialSize, bool resizeable)
            {
                Queue = new Queue<GameObject>(initialSize);
                Resizeable = resizeable;
            }
        }
    
        private static Dictionary<GameObject, Pool> poolDictionary;

        public PoolSetting[] settings;
        public bool autoPool;
        public int autoPoolSize;

        private void Awake()
        {
            poolDictionary = new Dictionary<GameObject, Pool>(settings.Length);
            foreach (var setting in settings)
            {
                CreatePool(setting.prefab, setting.size, setting.resizeable);
            }
        }

        public GameObject CreateFromPool(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            if (!poolDictionary.ContainsKey(prefab))
            { 
                if (!autoPool) 
                    throw new ArgumentException("Unknown pool " + gameObject.name + ".");
                CreatePool(prefab, autoPoolSize, false);
            }
        
            var pool = poolDictionary[prefab];
            var inst = pool.Queue.Dequeue();
            pool.Queue.Enqueue(inst);

            inst.SetActive(false);
            
            inst.transform.position = position;
            inst.transform.rotation = rotation;

            inst.SetActive(true);
            
            return inst;
        }

        public GameObject CreateFromPool(GameObject prefab, Transform t)
        {
            return CreateFromPool(prefab, t.position, t.rotation);
        }

        public void CreatePool(GameObject prefab, int initialSize, bool resizeable)
        {
            var pool = new Pool(initialSize, resizeable);
            poolDictionary.Add(prefab, pool);

            for (var i = 0; i < initialSize; i++)
            {
                var inst = Instantiate(prefab, transform);
                inst.SetActive(false);
            
                pool.Queue.Enqueue(inst);
            }
        }
    }
}
