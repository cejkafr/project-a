﻿using UnityEngine;

namespace Pools
{
    public interface IObjectPool
    {
        void CreatePool(GameObject prefab, int initialSize, bool resizeable);
        GameObject CreateFromPool(GameObject prefab, Vector3 position, Quaternion rotation);
        GameObject CreateFromPool(GameObject prefab, Transform t);
    }
}