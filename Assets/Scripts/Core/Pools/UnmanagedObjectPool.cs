﻿using System.Collections.Generic;
using UnityEngine;

namespace Pools
{
    public class UnmanagedObjectPool : MonoBehaviour
    {
        private readonly Dictionary<GameObject, Stack<IPooledObject>> pool
            = new Dictionary<GameObject, Stack<IPooledObject>>();

        public T CreateFromPool<T>(GameObject prefab, Transform parent) where T : IPooledObject
        {
            if (!pool.ContainsKey(prefab))
            {
                pool.Add(prefab, new Stack<IPooledObject>());
            }

            if (pool[prefab].Count == 0)
            {
                pool[prefab].Push(CreateInstance(prefab));
            }

            return pool[prefab].Pop().Recycle<T>(parent);
        }

        public T CreateFromPool<T>(GameObject prefab, Vector3 position, Quaternion rotation) where T : IPooledObject
        {
            if (!pool.ContainsKey(prefab))
            {
                pool.Add(prefab, new Stack<IPooledObject>());
            }

            if (pool[prefab].Count == 0)
            {
                pool[prefab].Push(CreateInstance(prefab));
            }

            return pool[prefab].Pop().Recycle<T>(position, rotation);
        }

        public void ReturnToPool(IPooledObject pooledObject)
        {
            pool[pooledObject.ArchType].Push(pooledObject);
        }

        private IPooledObject CreateInstance(GameObject prefab)
        {
            var inst = Instantiate(prefab, transform);
            inst.SetActive(false);
            var projectile = inst.GetComponent<IPooledObject>();
            projectile.ArchType = prefab;
            return projectile;
        }
    }
}