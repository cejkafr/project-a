﻿using UnityEngine;

namespace Pools
{
    public interface IPooledObject
    {
        GameObject ArchType { get; set; }

        T Recycle<T>(Vector3 pos, Quaternion rot) where T : IPooledObject;
        T Recycle<T>(Transform parent) where T : IPooledObject;
    }
}