﻿using System;
using UnityEngine;

public enum ItemBodySlotType
{
    BackR,
    ThighR,
}

[Serializable]
public struct ItemBodySlot
{
    public ItemBodySlotType type;
    public Transform transform;
}

[Serializable]
public struct ItemBodySlotOffset
{
    public ItemBodySlotType type;
    public Vector3 position;
    public Vector3 rotation;
}

public enum EquipmentSlotType : byte
{
    Primary,
    Sidearm,
    Grenade,
    Healing,
    None,
}