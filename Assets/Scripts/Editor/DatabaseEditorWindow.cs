﻿using System.Linq;
using System.Runtime.InteropServices;
using Databases;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class DatabaseEditorWindow : OdinMenuEditorWindow
    {
        private const string DatabasePath = "Assets/Database";

        [MenuItem("Game/Database")]
        private static void OpenWindow()
        {
            GetWindow<DatabaseEditorWindow>().Show();
        }

        private MotionEntry newMotionEntry;
        private MotionEntry newSoldierEntry;
        private MotionEntry newWeaponEntry;
        private CreateNewAmmoEntry newAmmoEntry;
        private MotionEntry newAmmoBoxEntry;
        private MotionEntry newThrowableEntry;

        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (newMotionEntry != null) DestroyImmediate(newMotionEntry);
            if (newSoldierEntry != null) DestroyImmediate(newSoldierEntry);
            if (newWeaponEntry != null) DestroyImmediate(newWeaponEntry);
            if (newAmmoEntry != null) DestroyImmediate(newAmmoEntry.ammoEntry);
            if (newAmmoBoxEntry != null) DestroyImmediate(newAmmoBoxEntry);
            if (newThrowableEntry != null) DestroyImmediate(newThrowableEntry);
        }

        protected override void OnBeginDrawEditors()
        {
            var selected = this.MenuTree.Selection;

            SirenixEditorGUI.BeginHorizontalToolbar();
            GUILayout.FlexibleSpace();
            if (SirenixEditorGUI.ToolbarButton("Delete selected"))
            {
                var asset = selected.SelectedValue as AmmoEntry;
                var path = AssetDatabase.GetAssetPath(asset);
                Debug.Log(path);
                // AssetDatabase.DeleteAsset(path);
                // AssetDatabase.SaveAssets();
            }
            SirenixEditorGUI.EndHorizontalToolbar();
            
            base.OnBeginDrawEditors();
            
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree
            {
                // DefaultMenuStyle = OdinMenuStyle.TreeViewStyle,
            };

            // tree.Add("Menu Style", tree.DefaultMenuStyle);

            tree.AddAllAssetsAtPath("Motions", "Assets/Database/Motions", typeof(MotionEntry));
            tree.AddAllAssetsAtPath("Damage Types", "Assets/Database/DamageTypes", typeof(DamageTypeEntry));
            tree.AddAllAssetsAtPath("Soldiers", "Assets/Database/Soldiers", typeof(SoldierEntry));
            tree.AddAllAssetsAtPath("Weapons", "Assets/Database/Weapons", typeof(WeaponEntry));
            tree.AddAllAssetsAtPath("Ammo", "Assets/Database/Ammo", typeof(AmmoEntry));
            tree.AddAllAssetsAtPath("Ammo Boxes", "Assets/Database/AmmoBoxes", typeof(AmmoBoxEntry));
            tree.AddAllAssetsAtPath("Throwables", "Assets/Database/Throwables", typeof(ThrowableEntry));
            tree.AddAllAssetsAtPath("LootBoxes", "Assets/Database/LootBoxes", typeof(LootBoxEntry));

            newAmmoEntry = new CreateNewAmmoEntry();
            tree.Add("Ammo/++ New ++", newAmmoEntry);
            
            tree.EnumerateTree().AddThumbnailIcons();

            return tree;
        }

        private class CreateNewAmmoEntry
        {
            public CreateNewAmmoEntry()
            {
                ammoEntry = CreateInstance<AmmoEntry>();
            }

            [InlineEditor(ObjectFieldMode = InlineEditorObjectFieldModes.Hidden)]
            public AmmoEntry ammoEntry;

            [Button("Save")]
            public void Create()
            {
                AssetDatabase.CreateAsset(ammoEntry, "Assets/Database/Ammo/" + ammoEntry.title + ".asset");
                AssetDatabase.SaveAssets();
                
                ammoEntry = CreateInstance<AmmoEntry>();
            }
        }
    }
}