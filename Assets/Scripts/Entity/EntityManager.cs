﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Entity
{
    public class EntityManager : IEntityManager
    {
        public event EntityManagerEvent OnCreate;
        public event EntityManagerEvent OnUpdate;
        public event EntityManagerEvent OnRemove;


        public readonly Dictionary<int, IEntity> Entities
            = new Dictionary<int, IEntity>(10);


        public void Create(IEntity entity, bool invokeEvent = true)
        {
            Entities.Add(entity.Id, entity);
            if (invokeEvent)
                OnCreate?.Invoke(entity);
        }

        public void Update(IEntity entity, bool invokeEvent = true)
        {
            if (invokeEvent)
                OnUpdate?.Invoke(entity);
        }

        public void Remove(IEntity entity, bool invokeEvent = true)
        {
            Entities.Remove(entity.Id);
            if (invokeEvent)
                OnRemove?.Invoke(entity);
        }


        public void Clear()
        {
            Entities.Clear();
        }


        public T GetById<T>(int id) where T : IEntity
        {
            if (!Entities.ContainsKey(id))
            {
                throw new Exception("There si no entity of id " + id);
            }

            return (T) Entities[id];
        }

        public List<IEntity> GetByOwner(PlayerId owner)
        {
            return Entities.Values.Where(entity => entity.Owner == owner).ToList();
        }

        public List<T> GetByOwner<T>(PlayerId owner) where T : IEntity
        {
            return Entities.Values.Where(entity => entity.Owner == owner).OfType<T>().ToList();
        }

        public T GetFirstByOwner<T>(PlayerId owner) where T : IEntity
        {
            return Entities.Values.Where(entity => entity.Owner == owner).OfType<T>().First();
        }
    }
}