﻿using Databases;
using Game.Inventory;
using UnityEngine;

namespace Game.Entity
{
    public class LootBox : AEntity<LootBoxEntry>, IInventoryEntity
    {
        public GameObject Prefab => Entry.prefab;
        public IInventory Inventory { get; }
        
        public LootBox(int id, LootBoxEntry entry, IInventory inventory, PlayerId owner = PlayerId.None) : base(id, entry, owner)
        {
            Inventory = inventory;
        }
    }
}