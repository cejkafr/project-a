﻿using System;
using Combat;
using Databases;

namespace Game.Entity
{
    [Serializable]
    public class Weapon : AItem<WeaponEntry>
    {
        public int Ammo { get; set; }
        public int AmmoMax => Entry.magazineSize;
        public bool IsFullyLoaded => Ammo == AmmoMax;
        public Damage Damage => Entry.damage;
        public float ReloadTime => Entry.motion.reloadTime * Entry.reloadTimeMult;
        public MotionEntry Motion => Entry.motion;

        public Weapon(int id, WeaponEntry entry, int ammo = 0) : base(id, ItemType.Weapon, entry)
        {
            Ammo = ammo;
        }
    }
}