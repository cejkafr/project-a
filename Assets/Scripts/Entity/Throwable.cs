﻿using System;
using Databases;

namespace Game.Entity
{
    [Serializable]
    public class Throwable : AItem<ThrowableEntry>
    {
        public float TriggerTime => Entry.triggerTime;

        public Throwable(int id, ThrowableEntry entry) : base(id, ItemType.Throwable, entry)
        {
        }
    }
}