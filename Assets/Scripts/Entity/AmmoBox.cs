﻿using Databases;

namespace Game.Entity
{
    public class AmmoBox : AItem<AmmoBoxEntry>
    {
        public AmmoBox(int id, AmmoBoxEntry entry) : base(id, ItemType.AmmoBox, entry)
        {
        }
    }
}