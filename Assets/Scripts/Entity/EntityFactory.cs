﻿using Game.Inventory;

namespace Game.Entity
{
    public class EntityFactory : IEntityFactory
    {
        private readonly DatabaseManager databaseManager;

        public IInventoryFactory InventoryFactory { get; set; }

        private int sequence;

        public EntityFactory(DatabaseManager databaseManager, IInventoryFactory inventoryFactory)
        {
            this.databaseManager = databaseManager;
            InventoryFactory = inventoryFactory;
        }

        public Soldier CreateSoldier(string id, int? entityId = null, PlayerId? ownerId = null)
        {
            var entry = databaseManager.GetSoldierById(id);
            var seqId = entityId ?? sequence++;
            var inventory = InventoryFactory.Create(seqId, entry.inventorySize);
            var entity = new Soldier(seqId, entry, inventory) {Owner = ownerId ?? PlayerId.None};
            return entity;
        }

        public Weapon CreateWeapon(string id, int? entityId = null, PlayerId? ownerId = null)
        {
            var entry = databaseManager.GetWeaponById(id);
            var entity = new Weapon(entityId ?? sequence++, entry, entry.magazineSize);
            entity.Owner = ownerId ?? PlayerId.None;
            return entity;
        }

        public AmmoBox CreateAmmoBox(string id, int? entityId = null, PlayerId? ownerId = null)
        {
            var entry = databaseManager.GetAmmoBoxById(id);
            var entity = new AmmoBox(entityId ?? sequence++, entry) {Owner = ownerId ?? PlayerId.None};
            return entity;
        }

        public Throwable CreateThrowable(string id, int? entityId = null, PlayerId? ownerId = null)
        {
            var entry = databaseManager.GetThrowableById(id);
            var entity = new Throwable(entityId ?? sequence++, entry) {Owner = ownerId ?? PlayerId.None};
            return entity;
        }

        public virtual LootBox CreateLootBox(string id, int? entityId = null, PlayerId? ownerId = null)
        {
            var entry = databaseManager.GetLootBoxById(id);
            var seqId = entityId ?? sequence++;
            var inventory = InventoryFactory.Create(seqId, entry.size);
            var entity = new LootBox(seqId, entry, inventory) {Owner = ownerId ?? PlayerId.None};
            return entity;
        }
    }
}