﻿using System;
using Databases;
using Game.Inventory;

namespace Game.Entity
{
    [Serializable]
    public class Soldier : AEntity<SoldierEntry>, ICombinedInventoryEntity
    {
        public float Health { get; set; }
        public int HealthMax => Entry.health;
        public float Stamina { get; set; }
        public float StaminaMax => Entry.stamina;
        public IInventory Inventory { get; }
        public IEquipment Equipment { get; }
        
        public Soldier(int id, SoldierEntry entry, IInventory inventory, float health = 0f) : base(id, entry)
        {
            Health = health > 0 ? health : entry.health;
            Stamina = entry.stamina;
            
            Inventory = inventory;
            Equipment = new StandardEquipment(id, entry.equipmentSlots);
        }
    }
}