﻿using Game.Entity;
using UnityEngine;

namespace Game.Networking
{
    public class EntityNetworkSync
    {
        private readonly IEntityManager entityManager;
        private readonly EntityFactory entityFactory;
        private readonly INetworkEventHandler eventHandler;
        private readonly InventoryNetworkSync inventoryNetworkSync;
        private readonly EquipmentNetworkSync equipmentNetworkSync;

        public EntityNetworkSync(IEntityManager entityManager, EntityFactory entityFactory, INetworkEventHandler eventHandler, 
            InventoryNetworkSync inventoryNetworkSync, EquipmentNetworkSync equipmentNetworkSync)
        {
            this.entityManager = entityManager;
            this.entityManager.OnCreate += CreateHandler;
            this.entityManager.OnUpdate += UpdateHandler;
            this.entityManager.OnRemove += DeleteHandler;
            this.entityFactory = entityFactory;
            this.eventHandler = eventHandler;
            this.eventHandler.RegisterListener(EntityNetworkSyncEvent.TypeCode, RemoteChange);
            this.inventoryNetworkSync = inventoryNetworkSync;
            this.equipmentNetworkSync = equipmentNetworkSync;
        }

        private void CreateHandler(IEntity entity)
        {
            switch (entity)
            {
                case Weapon _:
                {
                    var ev = new EntityNetworkSyncEvent(
                        EntityNetworkSyncEvent.ActionType.Created,
                        entity.Id,
                        entity.EntryId,
                        entity.Owner,
                        EntityNetworkSyncEvent.EntityType.Weapon
                    );
                    eventHandler.SendBuffered(ev);
                    break;
                }
                case Throwable _:
                {
                    var ev = new EntityNetworkSyncEvent(
                        EntityNetworkSyncEvent.ActionType.Created,
                        entity.Id,
                        entity.EntryId,
                        entity.Owner,
                        EntityNetworkSyncEvent.EntityType.Throwable
                    );
                    eventHandler.SendBuffered(ev);
                    break;
                }
                case AmmoBox _:
                {
                    var ev = new EntityNetworkSyncEvent(
                        EntityNetworkSyncEvent.ActionType.Created,
                        entity.Id,
                        entity.EntryId,
                        entity.Owner,
                        EntityNetworkSyncEvent.EntityType.Ammo
                    );
                    eventHandler.SendBuffered(ev);
                    break;
                }
                case LootBox e:
                {
                    // e.Inventory.OnChange += inventoryNetworkSync.LocalChange;
                    var ev = new EntityNetworkSyncEvent(
                        EntityNetworkSyncEvent.ActionType.Created,
                        entity.Id,
                        entity.EntryId,
                        entity.Owner,
                        EntityNetworkSyncEvent.EntityType.LootBox
                    );
                    eventHandler.SendBuffered(ev);
                    break;
                }
                case Soldier e:
                {
                    // e.Inventory.OnChange += inventoryNetworkSync.LocalChange;
                    e.Equipment.OnChange += equipmentNetworkSync.LocalChange;
                    var ev = new EntityNetworkSyncEvent(
                        EntityNetworkSyncEvent.ActionType.Created,
                        entity.Id,
                        entity.EntryId,
                        entity.Owner,
                        EntityNetworkSyncEvent.EntityType.Soldier
                    );
                    eventHandler.SendBuffered(ev);
                    break;
                }
            }
        }
        
        private void UpdateHandler(IEntity entity)
        {
            
        }
        
        private void DeleteHandler(IEntity entity)
        {
            
        }
        
        private void RemoteChange(object data)
        {
            var ev = new EntityNetworkSyncEvent(data);
            Debug.Log("Networking :: Sync :: Entity :: received event " + ev);

            switch (ev.Action)
            {
                case EntityNetworkSyncEvent.ActionType.Created
                    when ev.Type == EntityNetworkSyncEvent.EntityType.Soldier:
                {
                    var entity = entityFactory.CreateSoldier(ev.EntryId, ev.ItemId);
                    entity.Owner = ev.OwnerId;
                    entityManager.Create(entity, false);
                    break;
                }
                case EntityNetworkSyncEvent.ActionType.Created 
                    when ev.Type == EntityNetworkSyncEvent.EntityType.Weapon:
                {
                    var entity = entityFactory.CreateWeapon(ev.EntryId, ev.ItemId);
                    entity.Owner = ev.OwnerId;
                    entityManager.Create(entity, false);
                    break;
                }
                case EntityNetworkSyncEvent.ActionType.Created 
                    when ev.Type == EntityNetworkSyncEvent.EntityType.Throwable:
                {
                    var entity = entityFactory.CreateThrowable(ev.EntryId, ev.ItemId);
                    entity.Owner = ev.OwnerId;
                    entityManager.Create(entity, false);
                    break;
                }
                case EntityNetworkSyncEvent.ActionType.Created 
                    when ev.Type == EntityNetworkSyncEvent.EntityType.Ammo:
                {
                    var entity = entityFactory.CreateAmmoBox(ev.EntryId, ev.ItemId);
                    entity.Owner = ev.OwnerId;
                    entityManager.Create(entity, false);
                    break;
                }
                case EntityNetworkSyncEvent.ActionType.Created 
                    when ev.Type == EntityNetworkSyncEvent.EntityType.LootBox:
                {
                    var entity = entityFactory.CreateLootBox(ev.EntryId, ev.ItemId);
                    entity.Owner = ev.OwnerId;
                    entityManager.Create(entity, false);
                    break;
                }
                case EntityNetworkSyncEvent.ActionType.Updated:
                    break;
                case EntityNetworkSyncEvent.ActionType.Removed:
                    break;
            }
        }
    }
}