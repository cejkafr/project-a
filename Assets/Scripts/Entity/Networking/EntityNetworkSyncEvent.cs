﻿namespace Game.Networking
{
    public readonly struct EntityNetworkSyncEvent : INetworkEvent
    {
        public enum ActionType : byte
        {
            Created,
            Updated,
            Removed,
        }

        public enum EntityType : byte
        {
            Weapon,
            Ammo,
            Throwable,
            Soldier,
            LootBox,
        }

        public static byte TypeCode => (byte) NetworkEventType.SyncEntity;

        public byte Code => TypeCode;

        public readonly ActionType Action;
        public readonly int ItemId;
        public readonly string EntryId;
        public readonly PlayerId OwnerId;
        public readonly EntityType Type;
        public readonly object[] EntityData;

        public EntityNetworkSyncEvent(object data)
        {
            var arr = (object[]) data;
            Action = (ActionType) arr[0];
            ItemId = (int) arr[1];
            EntryId = (string) arr[2];
            OwnerId = (PlayerId) arr[3];
            Type = (EntityType) arr[4];
            EntityData = (object[]) arr[5];
        }

        public EntityNetworkSyncEvent(ActionType action, int itemId, string entryId, PlayerId ownerId, EntityType type)
        {
            Action = action;
            ItemId = itemId;
            EntryId = entryId;
            OwnerId = ownerId;
            Type = type;
            EntityData = null;
        }

        public EntityNetworkSyncEvent(ActionType action, int itemId, string entryId, PlayerId ownerId, EntityType type,
            object[] entityData)
        {
            Action = action;
            ItemId = itemId;
            EntryId = entryId;
            OwnerId = ownerId;
            Type = type;
            EntityData = entityData;
        }

        public object Serialize()
        {
            return new object[] {Action, ItemId, EntryId, OwnerId, Type, EntityData,};
        }

        public override string ToString()
        {
            return
                $"{nameof(Action)}: {Action}, {nameof(ItemId)}: {ItemId}, {nameof(EntryId)}: {EntryId}, {nameof(OwnerId)}: {OwnerId}, {nameof(Type)}: {Type}, {nameof(EntityData)}: {EntityData}, {nameof(Code)}: {Code}";
        }
    }
}