﻿using Game.Inventory;

namespace Game.Entity
{
    public interface IEntityFactory
    {
        Soldier CreateSoldier(string id, int? entityId = null, PlayerId? ownerId = null);
        Weapon CreateWeapon(string id, int? entityId = null, PlayerId? ownerId = null);
        AmmoBox CreateAmmoBox(string id, int? entityId = null, PlayerId? ownerId = null);
        Throwable CreateThrowable(string id, int? entityId = null, PlayerId? ownerId = null);
        LootBox CreateLootBox(string id, int? entityId = null, PlayerId? ownerId = null);
    }
}