﻿using System.Collections.Generic;
using Game.Entity;
using Game.Frontend.Controllers;
using UnityEngine;

namespace Frontend.UI
{
    public class DebugVicinityDetector : MonoBehaviour, IVicinityDetector
    {
        private readonly List<LootBox> data = new List<LootBox>();
        
        public void Set(LootBox lootBox)
        {
            data.Add(lootBox);
        }
        
        public void DetectLootBoxes(Vector3 position, List<LootBox> results)
        {
            results.AddRange(data);   
        }
    }
}