﻿using Frontend.UI;
using Game.Entity;
using Game.Inventory;
using UnityEngine;

namespace Game.Debugging
{
    public class InventoryDebug : MonoBehaviour
    {
        public DatabaseManager databaseManager;
        public GameUI main;
        public VicinityUI vicinity;

        private EntityFactory entityFactory;

        private void Start()
        {
            entityFactory = new EntityFactory(databaseManager, new GridInventoryFactory());

            var soldier = entityFactory.CreateSoldier("elMacho");
            var rifle = entityFactory.CreateWeapon("rifle0");
            soldier.Inventory.Insert(rifle);
            var pistol = entityFactory.CreateWeapon("pistol0");
            soldier.Equipment.Equip(pistol);

            main.PlayerInventory.SetEntity(soldier, transform);

            var vicinityDetector = GetComponent<DebugVicinityDetector>();
            vicinity.vicinityDetector = vicinityDetector;

            var lootBox = entityFactory.CreateLootBox("boxSmall");
            var pistol1 = entityFactory.CreateWeapon("pistol0");
            lootBox.Inventory.Insert(pistol1);
            vicinityDetector.Set(lootBox);
            
            var lootBox1 = entityFactory.CreateLootBox("boxLarge");
            var rifle1 = entityFactory.CreateWeapon("rifle0");
            lootBox1.Inventory.Insert(rifle1);
            vicinityDetector.Set(lootBox1);
            
            var lootBox2 = entityFactory.CreateLootBox("boxLarge");
            var rifle2 = entityFactory.CreateWeapon("rifle0");
            lootBox1.Inventory.Insert(rifle2);
            vicinityDetector.Set(lootBox2);
            
            Invoke(nameof(OpenInventory), 1);            
        }

        private void OpenInventory()
        {
            main.OpenInventory(true);
        }
    }
}