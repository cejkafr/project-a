﻿using System;
using System.Collections.Generic;
using Core;
using Extensions;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Frontend
{
    public class MaterialFxManager : SerializedSingleton<MaterialFxManager>
    {
        [SerializeField] private LevelEnvSettings levelEnvSettings;
        [SerializeField] private LayerMask characterLayer;
        [SerializeField] private List<GameObject> vfxBlood;
        [EventRef] [SerializeField] private string sfxFootsteps;
        [EventRef] [SerializeField] private string sfxImpact;

        private EnvFx envFxFallback;
        private readonly Dictionary<Material, EnvFx> envSettings = new Dictionary<Material, EnvFx>();
        private readonly Dictionary<string, EnvFx> materialCache = new Dictionary<string, EnvFx>(10);

        private void Awake()
        {
            envFxFallback = levelEnvSettings.envFx[0];
            foreach (var envFx in levelEnvSettings.envFx)
            {
                envSettings.Add(envFx.material, envFx);
            }
        }

        private EnvFx FindEnvFx(Material material)
        {
            return !envSettings.ContainsKey(material) ? envFxFallback : envSettings[material];
        }

        public EnvEventInstance CreateFootstepEventInstance()
        {
            return new EnvEventInstance(sfxFootsteps);
        }
        
        public EnvEventInstance CreateImpactEventInstance()
        {
            return new EnvEventInstance(sfxImpact);
        }

        public GameObject FindImpactVFX(GameObject go)
        {
            return characterLayer.Contains(go.layer) ? 
                vfxBlood[Random.Range(0, vfxBlood.Count)] : FindOrCreateInCache(go).impactFx;
        }

        private EnvFx FindOrCreateInCache(GameObject go)
        {
            if (materialCache.ContainsKey(go.name))
            {
                return materialCache[go.name];
            }

            var meshRenderer = go.GetComponent<MeshRenderer>();
            var envFx = meshRenderer != null ? FindEnvFx(meshRenderer.sharedMaterial) : envFxFallback;
            materialCache.Add(go.name, envFx);
            return envFx;
        }

        public void SetEnvSFX(GameObject go, ref EnvEventInstance sfxEvent)
        {
            if (go == null)
            {
                sfxEvent.SetWeights(envFxFallback);
                return;
            }
            if (characterLayer.Contains(go.layer))
            {
                sfxEvent.SetFlesh();
            }
            else
            {
                sfxEvent.SetWeights(FindOrCreateInCache(go));
            }
        }
    }

    public class EnvEventInstance
    {
        private EventInstance instance;
        private readonly PARAMETER_ID concrete;
        private readonly PARAMETER_ID gravel;
        private readonly PARAMETER_ID metal;
        private readonly PARAMETER_ID wood;
        private readonly PARAMETER_ID brick;
        private readonly PARAMETER_ID glass;
        private readonly PARAMETER_ID grass;
        private readonly PARAMETER_ID water;
        private readonly PARAMETER_ID rock;
        private readonly PARAMETER_ID flesh;

        public EnvEventInstance(string eventPath)
        {
            instance = RuntimeManager.CreateInstance(eventPath);

            var desc = RuntimeManager.GetEventDescription(eventPath);
            desc.getParameterDescriptionByName("Concrete", out var paramDesc);
            concrete = paramDesc.id;
            desc.getParameterDescriptionByName("Gravel", out paramDesc);
            gravel = paramDesc.id;
            desc.getParameterDescriptionByName("Metal", out paramDesc);
            metal = paramDesc.id;
            desc.getParameterDescriptionByName("Wood", out paramDesc);
            wood = paramDesc.id;
            desc.getParameterDescriptionByName("Brick", out paramDesc);
            brick = paramDesc.id;
            desc.getParameterDescriptionByName("Glass", out paramDesc);
            glass = paramDesc.id;
            desc.getParameterDescriptionByName("Grass", out paramDesc);
            grass = paramDesc.id;
            desc.getParameterDescriptionByName("Water", out paramDesc);
            water = paramDesc.id;
            desc.getParameterDescriptionByName("Rock", out paramDesc);
            rock = paramDesc.id;
            desc.getParameterDescriptionByName("Flesh", out paramDesc);
            flesh = paramDesc.id;
            
            SetPosition(Vector3.zero);
        }

        public void SetWeights(EnvFx envFx)
        {
            instance.setParameterByID(concrete, envFx.concreteWeight);
            instance.setParameterByID(gravel, envFx.gravelWeight);
            instance.setParameterByID(metal, envFx.metalWeight);
            instance.setParameterByID(wood, envFx.woodWeight);
            instance.setParameterByID(brick, envFx.brickWeight);
            instance.setParameterByID(glass, envFx.glassWeight);
            instance.setParameterByID(grass, envFx.grassWeight);
            instance.setParameterByID(water, envFx.waterWeight);
            instance.setParameterByID(rock, envFx.rockWeight);
            instance.setParameterByID(flesh, 0);
        }
        
        public void SetFlesh()
        {
            instance.setParameterByID(concrete,0);
            instance.setParameterByID(gravel, 0);
            instance.setParameterByID(metal, 0);
            instance.setParameterByID(wood, 0);
            instance.setParameterByID(brick, 0);
            instance.setParameterByID(glass,0);
            instance.setParameterByID(grass, 0);
            instance.setParameterByID(water, 0);
            instance.setParameterByID(rock, 0);
            instance.setParameterByID(flesh, 1);
        }

        public void SetPosition(Vector3 position)
        {
            instance.set3DAttributes(position.To3DAttributes());
        }

        public void SetVolume(float volume)
        {
            instance.setVolume(volume);
        }

        public void Start()
        {
            instance.start();
        }

        public void Release()
        {
            instance.release();
        }
    }
}