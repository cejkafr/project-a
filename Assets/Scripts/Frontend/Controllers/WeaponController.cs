﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Databases;
using FMOD.Studio;
using FMODUnity;
using Game.Entity;
using Photon.Pun;
using Pools;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class WeaponController : AEntityControllerBehaviour<Weapon>
    {
        #region Events

        public delegate void WeaponEvent();

        public delegate void WeaponShootEvent(Vector3 pos, Vector3 dir, bool fullAuto);

        public event WeaponEvent OnShootStart;
        public event WeaponEvent OnShootFinish;
        public event WeaponShootEvent OnShoot;
        public event WeaponEvent OnReloadStart;
        public event WeaponEvent OnReloadFinish;
        public event WeaponEvent OnReload;
        public event WeaponEvent OnEmpty;

        #endregion

        #region Params

        [SerializeField] public Transform gunOut;
        [SerializeField] private GameObject muzzlePrefab;
        [SerializeField] private GameObject bulletPrefab;
        [EventRef] [SerializeField] private string shootSfx;
        [EventRef] [SerializeField] private string emptySfx;
        [EventRef] [SerializeField] private string reloadSfx;
        [SerializeField] private Vector3 muzzleScale = Vector3.one;
        [SerializeField] private Vector3 impactScale = Vector3.one;
        [SerializeField] private Vector3 handOffsetPosition;
        [SerializeField] private Vector3 handOffsetRotation;
        [SerializeField] private ItemBodySlotOffset[] slotOffsets;

        private IObjectPool bulletObjectPool;
        private float shootDelay;
        private EventInstance shootSfxInstance;
        private EventInstance reloadSfxInstance;
        private EventInstance emptySfxInstance;

        #endregion

        public MotionEntry Motion => Entity.Motion;
        public int Ammo => Entity.Ammo;
        public int AmmoMax => Entity.AmmoMax;
        public float ReloadTime => Entity.ReloadTime;
        public bool IsFullAuto => Entity.Entry.fullAutoMode;
        public bool IsSemiAuto => Entity.Entry.semiAutoMode;
        public float ReloadTimeMult => Entity.Entry.reloadTimeMult;
        public bool IsTriggerPressed { get; private set; }

        #region Init

        private void OnDestroy()
        {
            shootSfxInstance.release();
            reloadSfxInstance.release();
            emptySfxInstance.release();
        }

        public void Init(IObjectPool pool, Weapon weapon)
        {
            Id = Entity = weapon;
            this.bulletObjectPool = pool;
            shootDelay = 1 / weapon.Entry.fireSpeed;

            shootSfxInstance = RuntimeManager.CreateInstance(shootSfx);
            shootSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
            reloadSfxInstance = RuntimeManager.CreateInstance(reloadSfx);
            reloadSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
            emptySfxInstance = RuntimeManager.CreateInstance(emptySfx);
            emptySfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
            // shootSfxInstance.set3DAttributes(RuntimeUtils.To3DAttributes(transform, default(Rigidbody)));
            // RuntimeManager.AttachInstanceToGameObject(shootSfxInstance, transform, default(Rigidbody));
        }

        public void EquipIn(IEnumerable<ItemBodySlot> slots)
        {
            var slot = slots.First(s => s.type == Entity.Entry.bodySlot);
            EquipIn(slot);
        }

        public void EquipIn(ItemBodySlot slot)
        {
            transform.parent = slot.transform;
            ApplySlotOffsets(slot.type);
        }

        public void EquipInHand(Transform handTransform)
        {
            transform.parent = handTransform;
            transform.localPosition = handOffsetPosition;
            transform.localRotation = Quaternion.Euler(handOffsetRotation);
        }

        private void ApplySlotOffsets(ItemBodySlotType slotType)
        {
            var slotOffset = slotOffsets.First(offset => offset.type == slotType);
            transform.localPosition = slotOffset.position;
            transform.localRotation = Quaternion.Euler(slotOffset.rotation);
        }

        #endregion

        #region Shoot

        private float lastShootTime;

        public bool SetTrigger(bool pressed)
        {
            IsTriggerPressed = pressed;

            if (!pressed)
            {
                return true;
            }

            if (!CanShoot())
            {
                return false;
            }
            if (Ammo <= 0)
            {
                NoAmmoFx();
                return false;
            }
            if (IsFullAuto)
            {
                StartCoroutine(_ShootFullAuto());
                return true;
            }

            if (IsSemiAuto)
            {
                StartCoroutine(_ShootSemiAuto());
                return true;
            }

            OnShootStart?.Invoke();
            Shoot(gunOut.position, transform.forward);
            OnShootFinish?.Invoke();
            return true;
        }
        
        public bool CanShoot()
        {
            return !reloading && !(Time.time - lastShootTime < shootDelay);
        }

        public void Shoot(Vector3 pos, Vector3 dir, bool fullAuto = false, int timeOffset = 0)
        {
            lastShootTime = Time.time;
            Entity.Ammo -= 1;
            
            shootSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
            shootSfxInstance.start();

            var muzzleInst = bulletObjectPool.CreateFromPool(
                muzzlePrefab, gunOut.position, Quaternion.LookRotation(dir));
            muzzleInst.transform.localScale = muzzleScale;

            var bullet = GameManager.BulletPool.CreateFromPool<ProjectileController>(
                bulletPrefab, pos, Quaternion.LookRotation(dir));
            bullet.speed = Entity.Entry.bulletSpeed;
            bullet.maxRange = Entity.Entry.maxRange;
            bullet.isBlank = !PhotonNetwork.IsMasterClient;
            bullet.rbForce = Entity.Entry.rbForce;
            bullet.impactScale = impactScale;
            bullet.damage = Entity.Damage;
            bullet.gameObject.SetActive(true);

            OnShoot?.Invoke(gunOut.position, transform.forward, fullAuto);
        }

        private IEnumerator _ShootSemiAuto()
        {
            return null;
        }

        private IEnumerator _ShootFullAuto()
        {
            var wfs = new WaitForSeconds(shootDelay);
            OnShootStart?.Invoke();
            while (IsTriggerPressed)
            {
                if (!reloading)
                {
                    if (Entity.Ammo <= 0)
                    {
                        NoAmmoFx();
                    }
                    else
                    {
                        Shoot(gunOut.position, transform.forward, true);
                    }
                }
                yield return wfs;
            }
            OnShootFinish?.Invoke();
        }

        #endregion

        #region Reload

        private bool reloading;

        public void NoAmmoFx()
        {
            shootSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
            emptySfxInstance.start();
            OnEmpty?.Invoke();
        }

        public bool Reload(int amount = -1)
        {
            if (amount == -1)
            {
                amount = Entity.Entry.magazineSize;
            }

            if (amount == 0 || Entity.IsFullyLoaded)
            {
                return false;
            }

            amount = Mathf.Min(amount, Entity.AmmoMax);
            ReloadFX(amount);
            return true;
        }

        public void ReloadFX(int amount)
        {
            StartCoroutine(_Reload(amount));
        }

        private IEnumerator _Reload(int amount)
        {
            reloading = true;
            OnReloadStart?.Invoke();

            if (Entity.Entry.perBulletReload)
            {
                for (var i = 0; i < amount; i++)
                {
                    reloadSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
                    reloadSfxInstance.start();
                    OnReload?.Invoke();
                    yield return new WaitForSeconds(ReloadTime);
                    Entity.Ammo++;
                }
            }
            else
            {
                reloadSfxInstance.set3DAttributes(gunOut.position.To3DAttributes());
                reloadSfxInstance.start();
                OnReload?.Invoke();
                yield return new WaitForSeconds(ReloadTime);
                Entity.Ammo = amount;
            }

            reloading = false;
            OnReloadFinish?.Invoke();
        }

        #endregion
    }
}