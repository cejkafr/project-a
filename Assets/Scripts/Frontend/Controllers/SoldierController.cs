﻿using Combat;
using Databases;
using FMODUnity;
using Frontend;
using Frontend.Behaviours;
using Game.Entity;
using Game.Inventory;
using Photon.Pun;
using Unity.Mathematics;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    [RequireComponent(typeof(RotationBehaviour))]
    [RequireComponent(typeof(MeleeBehaviour))]
    [RequireComponent(typeof(EquipBehaviour))]
    [RequireComponent(typeof(CombatBehaviour))]
    [RequireComponent(typeof(ClimbBehaviour))]
    public partial class SoldierController : AEntityControllerBehaviour<Soldier>, IHasHealth,
        IPunInstantiateMagicCallback, IPunObservable
    {

        #region Events

        public delegate void SoldierEvent();

        public delegate void SoldierTakeDamageEvent(Damage damage);

        public event SoldierTakeDamageEvent OnTakeDamage;
        public event SoldierEvent OnHealthChanged;
        public event SoldierEvent OnStaminaChanged;
        public event SoldierEvent OnDied;
        public event SoldierEvent OnRespawned;

        #endregion

        private Animator animator;

        private bool isSprinting;
        public Texture2D aimCursor;
        
        [SerializeField] private int entityId;

        private MotionEntry motion;

        public IInventory Inventory => Entity.Inventory;
        public IEquipment Equipment => Entity.Equipment;

        public MoveBehaviour MoveBehaviour { get; private set; }
        public RotationBehaviour RotationBehaviour { get; private set; }
        public MeleeBehaviour MeleeBehaviour { get; private set; }
        public CombatBehaviour CombatBehaviour { get; private set; }
        public EquipBehaviour EquipBehaviour { get; private set; }
        public ClimbBehaviour ClimbBehaviour { get; private set; }

        public EquipmentSlotType CurrentEquipment { get; private set; } = EquipmentSlotType.None;

        public bool IsArmed => EquipBehaviour.IsEquipped;
        public bool Aiming => CombatBehaviour.aiming;

        public int HealthMax => Entity.HealthMax;
        public float Health => Entity.Health;
        public bool IsDead { get; private set; }

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            entityId = (int) info.photonView.InstantiationData[0];
            Id = Entity = GameManager.EntityManager.GetById<Soldier>(entityId);
            Init();
        }

        #region Unity Functions

        private void Awake()
        {
            animator = GetComponent<Animator>();
            MoveBehaviour = GetComponent<MoveBehaviour>();
            RotationBehaviour = GetComponent<RotationBehaviour>();
            MeleeBehaviour = GetComponent<MeleeBehaviour>();
            CombatBehaviour = GetComponent<CombatBehaviour>();
            EquipBehaviour = GetComponent<EquipBehaviour>();
            ClimbBehaviour = GetComponent<ClimbBehaviour>();

            MoveBehaviour.OnMoveStart += OnMoveStart;
            MoveBehaviour.OnMoveFinish += OnMoveFinish;
            MoveBehaviour.OnFallStart += OnFallStart;
            MoveBehaviour.OnFallFinish += OnFallEnd;
            MeleeBehaviour.OnStart += OnMeleeStart;
            MeleeBehaviour.OnFinish += OnMeleeFinish;
            CombatBehaviour.OnAimStart += OnAimStart;
            CombatBehaviour.OnAimEnd += OnAimEnd;
            EquipBehaviour.OnStart += OnEquipStart;
            EquipBehaviour.OnFinish += OnEquipFinish;
            ClimbBehaviour.OnStart += OnClimbStart;
            ClimbBehaviour.OnFinish += OnClimbEnd;

            CombatBehaviour.OnShootStart += OnShootStart;
            CombatBehaviour.OnShootFinish += OnShootFinish;
            CombatBehaviour.OnShoot += OnShoot;
            CombatBehaviour.OnReloadStart += OnReloadStart;
            CombatBehaviour.OnReloadFinish += OnReloadFinish;
            // CombatBehaviour.OnEmpty += OnEmpty;

            if (!PhotonNetwork.IsConnected || !photonView.IsMine)
            {
                MoveBehaviour.enabled = false;
                RotationBehaviour.enabled = false;
            }
        }

        private void Start()
        {
            Equipment.OnChange += OnEquipmentChange;
            //
            // GetOrCreateController(Equipment.FindBySlotType<Weapon>(EquipmentSlotType.Primary));
            // GetOrCreateController(Equipment.FindBySlotType<Weapon>(EquipmentSlotType.Sidearm));
        }

        #endregion

        private void Init()
        {
            MoveBehaviour.speed = Entity.Entry.runSpeed * Entity.Entry.motion.moveSpeedMult;
            RotationBehaviour.speed = Entity.Entry.rotationSpeed * Entity.Entry.motion.rotationSpeedMult;
            
            CombatBehaviour.SetEnabled(false);
            CurrentEquipment = 0;

            ResetMotion();
            
            MeleeBehaviour.Init(Entity.Entry.meleeSfx);
        }

        private WeaponController GetOrCreateController(IItem item)
        {
            if (item.Type == ItemType.Weapon)
            {
                var controller = GameManager.ControllerManager.GetByEntity<WeaponController>(item);
                if (controller == null)
                {
                    controller = GameManager.ControllerFactory.CreateWeapon((Weapon) item, EquipBehaviour.itemBodySlots);
                }
                return controller;
            } else if (item.Type == ItemType.Throwable)
            {
                return null;
            }

            return null;
        }

        public void Sync(EquipmentSlotType currentEquipment, bool isArmed, bool isAiming)
        {
            CurrentEquipment = currentEquipment;
            var weapon = Equipment.FindBySlotType<Weapon>(CurrentEquipment);
            if (weapon != null)
            {
                CombatBehaviour.SetWeapon(GetOrCreateController(Equipment.FindBySlotType<Weapon>(CurrentEquipment)));
                if (isArmed)
                {
                    EquipRPC((byte) currentEquipment);
                }

                AimRPC(isAiming);
            }
        }

        #region Input Functions

        public void Look(Vector3 target)
        {
            if (CombatBehaviour.aiming || CombatBehaviour.TriggerInput)
            {
                RotationBehaviour.AimRotate(target);
            }
        }

        public void Move(Vector3 dir)
        {
            MoveBehaviour.Move(dir);
            if (!CombatBehaviour.aiming)
            {
                RotationBehaviour.Rotate(dir);
            }
        }

        public void Sprint(bool value)
        {
            isSprinting = value;
        }

        public void Climb()
        {
            ClimbBehaviour.Climb();
        }

        public void Use()
        {
        }

        public void Shoot(bool value)
        {
            CombatBehaviour.SetTrigger(value);

            if (value && EquipBehaviour.IsHolstered)
            {
                DoEquip();
            }
        }
        
        public void Aim(bool value)
        {
            DoAim(value);
            if (value && EquipBehaviour.IsHolstered)
            {
                DoEquip();
            }
        }

        public void MeleeAttack(bool heavy)
        {
            DoMelee(heavy);
        }

        public void Grenade(bool value)
        {
            // animator.SetBool(SoldierAnimator.Grenade, Input.GetKey(KeyCode.G));
            // if (Input.GetKeyDown(KeyCode.G))
            // {
            //     animator.SetTrigger(SoldierAnimator.GrenadeTrigger);
            // }
        }

        public void Reload()
        {
            // var ammo = Equipment.FindAmmo<AmmoBox>(CombatBehaviour.Weapon.Entity.Entry.ammo);
            // if (ammo == null)
            // {
            //     return;
            // }
            if (CombatBehaviour.Reload())
            {
                photonView.RPC(nameof(ReloadRPC), RpcTarget.Others, CombatBehaviour.Weapon.AmmoMax);
            }
        }

        public void Holster()
        {
            DoUnEquip();
        }

        public void HolsterSwitch()
        {
            if (EquipBehaviour.IsEquipped)
            {
                DoUnEquip();
            }
            else
            {
                DoEquip();
            }
        }

        public void Equip(EquipmentSlotType equipmentType)
        {
            var equipSlot = Equipment.FindBySlotType<Weapon>(equipmentType);
            if (CurrentEquipment != equipmentType && equipSlot != null)
            {
                if (EquipBehaviour.IsEquipped)
                {
                    DoUnEquip();
                }

                CurrentEquipment = equipmentType;
                CombatBehaviour.SetWeapon(GetOrCreateController(equipSlot));
                DoEquip();
            }
        }

        public void ApplyDamage(Damage damage)
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogWarning("Apply damage should not be called on clients.");
                return;
            }
            if (DoApplyDamage(damage))
            {
                photonView.RPC(nameof(ApplyDamageRPC), RpcTarget.Others, damage.DamageType.id, damage.Amount);
            }
            if (Health <= 0f)
            {
                Die();
                Invoke(nameof(Respawn), 10f);
            }
        }

        public void Die()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogWarning("Apply damage should not be called on clients.");
                return;
            }
            if (IsDead)
            {
                return;
            }
            if (DoDie())
            {
                photonView.RPC(nameof(DieRPC), RpcTarget.Others);
            }
        }

        public void Respawn()
        {
            if (!IsDead)
            {
                return;
            }

            if (DoRespawn())
            {
                photonView.RPC(nameof(RespawnRPC), RpcTarget.Others);
            }
        }

        #endregion

        public void ResetMotion()
        {
            var prevMotion = motion;
            motion = EquipBehaviour.IsEquipped ? EquipBehaviour.item.Motion : Entity.Entry.motion;
            if (prevMotion == null || prevMotion.id != motion.id)
            {
                animator.SetInteger(SoldierAnimator.MotionId, motion.id);
                animator.SetTrigger(SoldierAnimator.ChangeMotion);
            }
        }

        private void SetUserInputActive(bool value)
        {
            MoveBehaviour.disabled = !value;
            RotationBehaviour.disabled = !value;
            CombatBehaviour.SetEnabled(value);
            MeleeBehaviour.disabled = !value;
            ClimbBehaviour.disabled = !value;
        }

        #region Behaviour Events

        private void OnShootStart()
        {
            RotationBehaviour.speed = Entity.Entry.aimRotationSpeed * Entity.Entry.motion.rotationSpeedMult;
        }

        private void OnShootFinish()
        {
            if (!CombatBehaviour.aiming)
            {
                RotationBehaviour.speed = Entity.Entry.rotationSpeed * Entity.Entry.motion.rotationSpeedMult;
            }
        }

        private void OnShoot(Vector3 pos, Vector3 dir, bool continuous)
        {
            if (photonView.IsMine)
            {
                photonView.RPC(nameof(ShootRPC), RpcTarget.Others, pos, dir);
            }
        }

        private void OnReloadStart()
        {
        }

        private void OnReloadFinish()
        {
        }

        private void OnMeleeStart()
        {
            SetUserInputActive(false);
        }

        private void OnMeleeFinish()
        {
            SetUserInputActive(true);
        }

        private void OnEquipStart()
        {
        }

        private void OnEquipFinish()
        {
            CombatBehaviour.SetWeapon(GetOrCreateController(
                Equipment.FindBySlotType<Weapon>(CurrentEquipment)));
            CombatBehaviour.SetEnabled(EquipBehaviour.IsEquipped);
            ResetMotion();
            if (!CombatBehaviour.aiming)
            {
                MoveBehaviour.speed = Entity.Entry.runSpeed * motion.moveSpeedMult;
                RotationBehaviour.speed = Entity.Entry.rotationSpeed * motion.rotationSpeedMult;
            }
        }

        private void OnAimStart()
        {
            MoveBehaviour.aim = true;
            MoveBehaviour.speed = Entity.Entry.walkSpeed * motion.moveSpeedMult;
            RotationBehaviour.speed = Entity.Entry.aimRotationSpeed * motion.rotationSpeedMult;
            if (PhotonNetwork.IsConnected && photonView.IsMine)
            {
                Cursor.SetCursor(aimCursor, Vector2.zero, CursorMode.Auto);
            }
        }

        private void OnAimEnd()
        {
            MoveBehaviour.aim = false;
            MoveBehaviour.speed = Entity.Entry.runSpeed * motion.moveSpeedMult;
            if (!CombatBehaviour.TriggerInput)
            {
                RotationBehaviour.speed = Entity.Entry.rotationSpeed * motion.rotationSpeedMult;
            }

            if (PhotonNetwork.IsConnected && photonView.IsMine)
            {
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            }
        }

        private void OnMoveStart()
        {
        }

        private void OnMoveFinish()
        {
        }

        private void OnFallStart()
        {
            SetUserInputActive(false);
        }

        private void OnFallEnd()
        {
            SetUserInputActive(true);
        }

        private void OnClimbStart()
        {
            SetUserInputActive(false);
        }

        private void OnClimbEnd()
        {
            SetUserInputActive(true);
        }

        private void OnEquipmentChange(IEquipment equipment, InventoryAction action, int2 position, IItem item)
        {
            if (item.Type != ItemType.Weapon)
            {
                return;
            }
            var weapon = GetOrCreateController(item);
            if (action == InventoryAction.Remove && !Equipment.Find(item))
            {
                weapon.gameObject.SetActive(false);
            }
            else
            {
                if (weapon == null)
                {
                    GameManager.ControllerFactory.CreateWeapon((Weapon) item, EquipBehaviour.itemBodySlots);
                }
                else
                {
                    weapon.gameObject.SetActive(true);
                }
            }
        }

        #endregion

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext((byte) Mathf.CeilToInt(Entity.Health));
            }
            else
            {
                Entity.Health = (byte) stream.ReceiveNext();
            }
        }
    }
}