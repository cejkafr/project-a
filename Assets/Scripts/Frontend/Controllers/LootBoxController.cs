﻿using Game.Entity;
using Game.Inventory;
using Photon.Pun;

namespace Game.Frontend.Controllers
{
    public class LootBoxController : AEntityControllerBehaviour<LootBox>, IPunInstantiateMagicCallback, IPunObservable
    {
        public int entityId;
        
        public IInventory Inventory => Entity.Inventory;

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            entityId = (int) info.photonView.InstantiationData[0];
            Id = Entity = GameManager.EntityManager.GetById<LootBox>(entityId);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {

        }
    }
}