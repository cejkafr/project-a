﻿using Cinemachine;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class FollowCameraController : MonoBehaviour
    {
        private CinemachineVirtualCamera virtualCamera;
        private CinemachineBasicMultiChannelPerlin cameraPerlinModule;

        private void Awake()
        {
            virtualCamera = GetComponent<CinemachineVirtualCamera>();
            cameraPerlinModule = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        public void Shake(float intensity)
        {
            cameraPerlinModule.enabled = true;
            cameraPerlinModule.m_AmplitudeGain = intensity;

        }

        public void CancelShake()
        {
            cameraPerlinModule.enabled = false;
            cameraPerlinModule.m_AmplitudeGain = 0;
        }

        public void SetTarget(Transform t)
        {
            virtualCamera.Follow = t;
            virtualCamera.LookAt = t;
        }

        public void SetPriority(int value)
        {
            virtualCamera.Priority = value;
        }
        
    }
}