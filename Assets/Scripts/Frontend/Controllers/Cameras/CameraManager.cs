﻿using FMODUnity;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class CameraManager : MonoBehaviour
    {
        public static CameraManager Instance { get; private set; }

        public Camera realCamera;
        public FollowCameraController followCameraController;

        private StudioListener studioListener;
        
        private void Awake()
        {
            if (Instance != null)
            {
                print("CameraManager already exists, destroying.");
                Destroy(gameObject);
                return;
            }

            Instance = this;

            studioListener = realCamera.GetComponent<StudioListener>();
        }

        public void SetPlayer(SoldierController controller)
        {
            followCameraController.SetTarget(controller.transform);
            studioListener.attenuationObject = controller.gameObject;
        }

        public void Shake(float intensity, float time)
        {
            CancelInvoke(nameof(CancelShake));
            Invoke(nameof(CancelShake), time);
            followCameraController.Shake(intensity);
        }

        public void CancelShake()
        {
            followCameraController.CancelShake();
        }
    }
}