﻿using Cinemachine;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class ThirdPersonCameraController : MonoBehaviour
    {
        private new Camera camera;
        private CinemachineFreeLook virtualCamera;
        private CinemachineBasicMultiChannelPerlin[] cameraPerlinModules;

        private void Awake()
        {
            virtualCamera = GetComponent<CinemachineFreeLook>();
            cameraPerlinModules = new[]
            {
                virtualCamera.GetRig(0).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>(),
                virtualCamera.GetRig(1).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>(),
                virtualCamera.GetRig(2).GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>(),
            };
        }

        public void Input(ref Vector2 delta)
        {
            virtualCamera.m_XAxis.m_InputAxisValue = delta.x;
            virtualCamera.m_YAxis.m_InputAxisValue = delta.y;
        }

        public void Shake(float intensity)
        {
            foreach (var perlinModule in cameraPerlinModules)
            {
                perlinModule.enabled = true;
                perlinModule.m_AmplitudeGain = intensity;
            }
        }

        public void CancelShake()
        {
            foreach (var perlinModule in cameraPerlinModules)
            {
                perlinModule.enabled = false;
                perlinModule.m_AmplitudeGain = 0;
            }
        }

        public void SetTarget(Transform t)
        {
            virtualCamera.Follow = t;
            virtualCamera.LookAt = t;
        }

        public void SetPriority(int value)
        {
            virtualCamera.Priority = value;
        }
    }
}