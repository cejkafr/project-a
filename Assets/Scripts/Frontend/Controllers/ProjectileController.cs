﻿using System;
using Combat;
using FMOD.Studio;
using FMODUnity;
using Frontend;
using Pools;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class ProjectileController : MonoBehaviour, IPooledObject
    {
        public float speed;
        public float maxRange;
        public Vector3 impactScale = Vector3.one;
        public bool isBlank = true;
        public float rbForce = 200;
        public Damage damage;
        public LayerMask layerMask;

        private EnvEventInstance eventInstance;
        private TrailRenderer trailRenderer;
        private float totalDistTravelled;
        private float frameDistTravelled;
        private Vector3 translation;
        private RaycastHit hit;
        private Transform t;

        public GameObject ArchType { get; set; }

        private void Awake()
        {
            trailRenderer = GetComponent<TrailRenderer>();
            t = transform;
            eventInstance = MaterialFxManager.Instance.CreateImpactEventInstance();
        }

        private void OnDestroy()
        {
            eventInstance.Release();
        }

        public T Recycle<T>(Vector3 pos, Quaternion rot) where T : IPooledObject
        {
            t.position = pos;
            t.rotation = rot;
            return (T) (IPooledObject) this;
        }

        public T Recycle<T>(Transform parent) where T : IPooledObject
        {
            throw new System.NotImplementedException();
        }

        private void OnEnable()
        {
            totalDistTravelled = 0f;
        }

        public void Update()
        {
            translation = Vector3.forward * (speed * Time.deltaTime);
            frameDistTravelled = translation.magnitude;
            if (Physics.Raycast(t.position, t.forward, out hit,
                frameDistTravelled, layerMask, QueryTriggerInteraction.Ignore))
            {
                HitTarget();
            }

            totalDistTravelled += frameDistTravelled;
            if (totalDistTravelled > maxRange)
            {
                Deactivate();
            }

            t.Translate(translation);
        }

        private void HitTarget()
        {
            var rot = Quaternion.LookRotation(hit.normal.normalized - t.forward.normalized);
            var impactPrefab = MaterialFxManager.Instance.FindImpactVFX(hit.transform.gameObject);
            var impactInst = GameManager.VFXPool.CreateFromPool(impactPrefab, hit.point, rot);
            impactInst.transform.localScale = impactScale;

            eventInstance.Start();
            eventInstance.SetPosition(hit.point);
            MaterialFxManager.Instance.SetEnvSFX(hit.transform.gameObject, ref eventInstance);

            if (!isBlank)
            {
                if (rbForce > 0f && hit.rigidbody != null)
                {
                    hit.rigidbody.AddForceAtPosition(hit.normal * (-1 * rbForce), hit.point,
                        ForceMode.Force);
                }

                var hasHealth = hit.transform.GetComponent<IHasHealth>();
                hasHealth?.ApplyDamage(damage);
            }

            Deactivate();
        }

        private void Deactivate()
        {
            gameObject.SetActive(false);
            trailRenderer.Clear();
            GameManager.BulletPool.ReturnToPool(this);
        }
    }
}