﻿using Frontend;
using Frontend.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Game.Frontend.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private InputActionAsset controls;
        [SerializeField] private GameUI gameUI;

        private IPlaneRaycaster raycaster;
        private SoldierController player;

        private InputActionMap mission;
        private InputActionMap ui;

        private Vector2 dir;
        private Vector2 mousePos;
        private bool heavyAttack;

        private void Awake()
        {
            raycaster = GetComponent<IPlaneRaycaster>();

            mission = controls.FindActionMap("Player");
            ui = controls.FindActionMap("UI");
            
            BindUIControls();
        }

        private void BindUIControls()
        {
            var inventoryUi = ui.FindAction("Inventory");
            inventoryUi.started += InventoryUi;
            var closeUi = ui.FindAction("Close");
            closeUi.started += CloseUi;
        }

        public void SetPlayer(SoldierController controller)
        {
            player = controller;

            
            var look = mission.FindAction("Look");
            look.performed += Look;
            look.canceled += Look;
            var move = mission.FindAction("Move");
            move.performed += Move;
            move.canceled += Move;
            var sprint = mission.FindAction("Sprint");
            sprint.performed += Sprint;
            sprint.canceled += Sprint;
            var climb = mission.FindAction("Jump");
            climb.performed += Climb;
            var use = mission.FindAction("Use");
            use.performed += Use;
            var fire = mission.FindAction("Fire");
            fire.performed += Fire;
            fire.canceled += Fire;
            var aim = mission.FindAction("Aim");
            aim.performed += Aim;
            aim.canceled += Aim;
            var reload = mission.FindAction("Reload");
            reload.performed += Reload;
            var grenade = mission.FindAction("Grenade");
            grenade.performed += Grenade;
            grenade.canceled += Grenade;
            var melee = mission.FindAction("Melee");
            melee.performed += MeleeAttack;
            var holster = mission.FindAction("Holster");
            holster.performed += Holster;
            var quickSlot1 = mission.FindAction("Quick Slot 1");
            quickSlot1.performed += QuickSlot1;
            var quickSlot2 = mission.FindAction("Quick Slot 2");
            quickSlot2.performed += QuickSlot2;
            var inventory = mission.FindAction("Inventory");
            inventory.started += Inventory;

            SetUIEnabled(false);
        }

        public void SetUIEnabled(bool value)
        {
            if (value)
            {
                mission.Disable();
                ui.Enable();
            }
            else
            {
                mission.Enable();
                ui.Disable();
            }
        }

        private void Look(InputAction.CallbackContext ctx)
        {
            mousePos = ctx.ReadValue<Vector2>();
            player.Look(raycaster.CursorPosition(mousePos));
        }

        private void Move(InputAction.CallbackContext ctx)
        {
            dir = ctx.ReadValue<Vector2>();
            player.Move(new Vector3(dir.x, 0f, dir.y));
        }

        private void Sprint(InputAction.CallbackContext ctx)
        {
            heavyAttack = ctx.ReadValueAsButton();
            player.Sprint(heavyAttack);
        }

        private void Climb(InputAction.CallbackContext ctx)
        {
            player.Climb();
        }

        private void Use(InputAction.CallbackContext ctx)
        {
            player.Use();
        }

        private void Fire(InputAction.CallbackContext ctx)
        {
            player.Shoot(ctx.ReadValueAsButton());
        }

        private void Aim(InputAction.CallbackContext ctx)
        {
            player.Aim(ctx.ReadValueAsButton());
        }

        private void Grenade(InputAction.CallbackContext ctx)
        {
            player.Grenade(ctx.ReadValueAsButton());
        }

        private void Reload(InputAction.CallbackContext ctx)
        {
            player.Reload();
        }

        private void MeleeAttack(InputAction.CallbackContext ctx)
        {
            player.MeleeAttack(heavyAttack);
        }

        private void Holster(InputAction.CallbackContext ctx)
        {
            player.HolsterSwitch();
        }

        private void QuickSlot1(InputAction.CallbackContext ctx)
        {
            player.Equip(EquipmentSlotType.Primary);
        }

        private void QuickSlot2(InputAction.CallbackContext ctx)
        {
            player.Equip(EquipmentSlotType.Sidearm);
        }
        
        private void QuickSlot3(InputAction.CallbackContext ctx)
        {
            // player.Equip(EquipmentSlotType.Sidearm);
        }

        private void Inventory(InputAction.CallbackContext ctx)
        {
            player.Holster();
            gameUI.OpenInventory(true);
        }

        private void InventoryUi(InputAction.CallbackContext ctx)
        {
            gameUI.OpenInventory(false);
        }

        private void CloseUi(InputAction.CallbackContext ctx)
        {
            gameUI.CloseAllWindows();
        }
    }
}