﻿using Game.Entity;
using Pools;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class GrenadeController : AEntityControllerBehaviour<Throwable>
    {
        public GameObject explosionPrefab;
        public float triggerTime = 3f;

        private void Start()
        {
            Invoke(nameof(Explode), triggerTime);
        }

        private void OnDisable()
        {
            CancelInvoke(nameof(Explode));
        }

        public void Init(Throwable entity)
        {
            
        }
        
        private void Explode()
        {
            var pool = FindObjectOfType<QueueObjectPool>();
            pool.CreateFromPool(explosionPrefab, transform.position, Quaternion.identity);
            CameraManager.Instance.Shake(5, 0.6f);
            gameObject.SetActive(false);
        }
    }
}