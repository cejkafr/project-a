﻿using System.Collections.Generic;
using System.Linq;
using Game.Entity;
using Pools;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class ControllerFactory : MonoBehaviour
    {
        [SerializeField] private ControllerManager manager;
        [SerializeField] private QueueObjectPool queuePool;
    
        public GrenadeController CreateGrenade(Throwable throwable, Vector3 pos, Quaternion rot)
        {
            var inst = Instantiate(throwable.Prefab, pos, rot);
            var controller = inst.GetComponent<GrenadeController>();
            controller.Init(throwable);
            manager.Register(controller);
            return controller;
        }
        
        // public LootBoxController CreateLootBox(LootBox lootBox, Vector3 pos, Quaternion rot)
        // {
        //     var inst = Instantiate(lootBox.Prefab, pos, rot);
        //     var controller = inst.GetComponent<LootBoxController>();
        //     manager.Register(controller);
        //     return controller;
        // }

        
        public WeaponController CreateWeapon(Weapon weapon, Vector3 pos, Quaternion rot)
        {
            var inst = Instantiate(weapon.Prefab, pos, rot);
            var controller = inst.GetComponent<WeaponController>();
            controller.Init(queuePool, weapon);
            manager.Register(controller);
            return controller;
        }

        public WeaponController CreateWeapon(Weapon weapon, IEnumerable<ItemBodySlot> slots)
        {
            var slot = slots.First(s => s.type == weapon.Entry.bodySlot);
            var inst = Instantiate(weapon.Prefab, slot.transform);
            var controller = inst.GetComponent<WeaponController>();
            controller.Init(queuePool, weapon);
            controller.EquipIn(slot);
            manager.Register(controller);
            return controller;
        }
    
        public WeaponController CreateWeaponInHand(Weapon weapon, Transform handTransform)
        {
            var inst = Instantiate(weapon.Prefab, handTransform);
            var controller = inst.GetComponent<WeaponController>();
            controller.Init(queuePool, weapon);
            controller.EquipInHand(handTransform);
            manager.Register(controller);
            return controller;
        }
    }
}