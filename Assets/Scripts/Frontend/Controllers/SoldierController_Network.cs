﻿using Combat;
using FMODUnity;
using Frontend;
using Game.Entity;
using Photon.Pun;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public partial class SoldierController
    {
        #region RPC Wrappers

        private void DoAim(bool value)
        {
            if (CombatBehaviour.SetAim(value, Entity.Entry.aimSpeed))
            {
                photonView.RPC(nameof(AimRPC), RpcTarget.Others, value);
            }
        }

        private void DoShoot(bool value)
        {
        }

        private void DoMelee(bool heavy)
        {
            var attack = motion.meleeAttacks[heavy ? 1 : 0];
            if (MeleeBehaviour.Attack(attack))
            {
                photonView.RPC(nameof(MeleeRPC), RpcTarget.Others, transform.position, transform.forward, heavy);
            }
        }

        private bool DoApplyDamage(Damage damage)
        {
            if (IsDead)
            {
                return false;
            }
            Entity.Health -= damage.Amount;
            RuntimeManager.PlayOneShot(Entity.Entry.hurtSfx, transform.position);
            OnHealthChanged?.Invoke();
            OnTakeDamage?.Invoke(damage);
            return true;
        }
        
        private bool DoDie()
        {
            if (IsDead)
            {
                return false;
            }
            SetUserInputActive(false);
            Entity.Health = 0;
            IsDead = true;
            animator.SetBool(SoldierAnimator.IsDead, true);
            animator.SetTrigger(SoldierAnimator.Die);
            RuntimeManager.PlayOneShot(Entity.Entry.deathSfx, transform.position);
            OnDied?.Invoke();
            return true;
        }

        private bool DoRespawn()
        {
            if (!IsDead)
            {
                return false;
            }
            SetUserInputActive(true);
            Entity.Health = Entity.HealthMax;
            IsDead = false;
            animator.SetBool(SoldierAnimator.IsDead, false);
            OnHealthChanged?.Invoke();
            OnRespawned?.Invoke();
            return true; 
        }
        
        private void DoEquip()
        {
            var weapon = GetOrCreateController(
                Equipment.FindBySlotType<Weapon>(CurrentEquipment));
            if (EquipBehaviour.Equip(weapon))
            {
                photonView.RPC(nameof(EquipRPC), RpcTarget.Others, CurrentEquipment);
            }
        }

        private void DoUnEquip()
        {
            var weapon = GetOrCreateController(
                Equipment.FindBySlotType<Weapon>(CurrentEquipment));
            if (EquipBehaviour.UnEquip(weapon))
            {
                photonView.RPC(nameof(UnEquipRPC), RpcTarget.Others, CurrentEquipment);
            }
        }

        #endregion
        
        #region RPC

        [PunRPC]
        public void ShootRPC(Vector3 pos, Vector3 dir, PhotonMessageInfo msg)
        {
            print("Networking :: RPC :: Shoot :: Boom boom.");
            var timeOffset = PhotonNetwork.ServerTimestamp - msg.SentServerTimestamp;
            CombatBehaviour.Weapon.Shoot(pos, dir, false, timeOffset);
        }
        
        [PunRPC]
        public void AimRPC(bool value)
        {
            print("Networking :: RPC :: Aim :: " + value);
            DoAim(value);
        }

        [PunRPC]
        public void MeleeRPC(Vector3 pos, Vector3 dir, bool heavy, PhotonMessageInfo msg)
        {
            print("Networking :: RPC :: Melee :: " + heavy);
            var timeOffset = PhotonNetwork.ServerTimestamp - msg.SentServerTimestamp;
            var attack = motion.meleeAttacks[heavy ? 1 : 0];
            MeleeBehaviour.AttackFX(attack);
        }
        
        [PunRPC]
        public void ReloadRPC(int amount)
        {
            print("Networking :: RPC :: Reload :: " + amount);
            CombatBehaviour.Weapon.ReloadFX(amount);
        }

        [PunRPC]
        public void EquipRPC(byte currentEquipment)
        {
            CurrentEquipment = (EquipmentSlotType) currentEquipment;
            print("Networking :: RPC :: Equip :: " + CurrentEquipment);
            var slot = GetOrCreateController(
                Equipment.FindBySlotType<Weapon>(CurrentEquipment));
            CombatBehaviour.SetWeapon(slot);
            EquipBehaviour.Equip(slot);
        }

        [PunRPC]
        public void UnEquipRPC(byte currentEquipment)
        {
            CurrentEquipment = (EquipmentSlotType) currentEquipment;
            print("Networking :: RPC :: UnEquip :: " + CurrentEquipment);
            var slot = GetOrCreateController(
                Equipment.FindBySlotType<Weapon>(CurrentEquipment));
            EquipBehaviour.UnEquip(slot);
        }
        
        [PunRPC]
        public void ApplyDamageRPC(int damageTypeId, float amount)
        {
            var damageType = GameManager.DatabaseManager.GetDamageTypeById(damageTypeId);
            var damage = new Damage(damageType, amount);
            print("Networking :: RPC :: ApplyDamage :: " + damage);
            DoApplyDamage(damage);
        }
        
        [PunRPC]
        public void DieRPC()
        {
            print("Networking :: RPC :: Die :: Just die already");
            DoDie();
        }
        
        [PunRPC]
        public void RespawnRPC()
        {
            print("Networking :: RPC :: Respawn :: Are you alive?");
            Respawn();
        }

        #endregion
    }
}