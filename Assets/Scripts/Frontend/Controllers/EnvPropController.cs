﻿using Combat;
using Databases;
using Photon.Pun;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class EnvPropController : MonoBehaviour
    {
        public DamageTypeEntry damageType;
        public float damageMult = 1;

        private Rigidbody rb;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }
            var health = other.gameObject.GetComponent<IHasHealth>();
            if (health == null)
            {
                return;
            }
            var damage = rb.velocity.magnitude * rb.mass * damageMult;
            health.ApplyDamage(new Damage(damageType, damage));
        }
    }
}