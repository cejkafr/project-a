﻿using System;
using System.Collections.Generic;
using Game.Entity;
using UnityEngine;

namespace Game.Frontend.Controllers
{
    public class ControllerManager : MonoBehaviour
    {
        private readonly Dictionary<IEntity, IEntityController> controllers 
            = new Dictionary<IEntity, IEntityController>(10);

        public void Register(IEntityController controller)
        {
            controllers.Add(controller.Id, controller);
        }

        public void Unregister(IEntityController controller)
        {
            controllers.Remove(controller.Id);
        }

        public T GetByEntity<T>(IEntity entity) where T : MonoBehaviour
        {
            if (entity == null)
            {
                throw new Exception("Cannot get controller for null entity.");
            }

            if (!controllers.ContainsKey(entity))
            {
                return null;
            }
            return (T) controllers[entity];
        }
    }
}