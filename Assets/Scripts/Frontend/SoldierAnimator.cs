﻿using UnityEngine;

namespace Frontend
{
    public static class SoldierAnimator
    {
        public static readonly int Aim = Animator.StringToHash("Aim");
        public static readonly int InputMagnitude = Animator.StringToHash("InputMagnitude");
        public static readonly int Vertical = Animator.StringToHash("Horizontal");
        public static readonly int Horizontal = Animator.StringToHash("Vertical");
        public static readonly int Shoot = Animator.StringToHash("Fire");
        public static readonly int ShootContinuous = Animator.StringToHash("Fire");
        public static readonly int Reload = Animator.StringToHash("Reload");
        public static readonly int HorAimAngle = Animator.StringToHash("HorAimAngle");
        public static readonly int VerAimAngle = Animator.StringToHash("VerAimAngle");
        public static readonly int Grenade = Animator.StringToHash("Grenade");
        public static readonly int GrenadeTrigger = Animator.StringToHash("GrenadeTrigger");
        public static readonly int GrenadeThrow = Animator.StringToHash("GrenadeThrow");
        public static readonly int GrenadeThrowType = Animator.StringToHash("GrenadeThrowType");
        public static readonly int Melee = Animator.StringToHash("Melee");
        public static readonly int MeleeType = Animator.StringToHash("MeleeType");
        public static readonly int EquipWeapon = Animator.StringToHash("EquipWeapon");
        public static readonly int UnEquipWeapon = Animator.StringToHash("UnequipWeapon");
        public static readonly int ChangeMotion = Animator.StringToHash("SwapWeapon");
        public static readonly int MotionId = Animator.StringToHash("WeaponId");
        public static readonly int Climb = Animator.StringToHash("Climb");
        public static readonly int Grounded = Animator.StringToHash("Grounded");
        public static readonly int Fall = Animator.StringToHash("Fall");
        public static readonly int Land = Animator.StringToHash("Land");
        public static readonly int LandMode = Animator.StringToHash("LandMode");
        public static readonly int EquipSpeed = Animator.StringToHash("EquipSpeed");
        public static readonly int Die = Animator.StringToHash("Die");
        public static readonly int IsDead = Animator.StringToHash("Dead");
        public static readonly int IsRightLegUp = Animator.StringToHash("isRightLegUp");
    }
}