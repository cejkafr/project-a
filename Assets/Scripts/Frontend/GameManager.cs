﻿using DG.Tweening;
using Game.Entity;
using Game.Frontend.Controllers;
using Game.Inventory;
using Pools;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public static DatabaseManager DatabaseManager { get; private set; }
    public static EntityManager EntityManager { get; private set; }
    public static EntityFactory EntityFactory { get; private set; }
    public static ControllerFactory ControllerFactory { get; private set; }
    public static ControllerManager ControllerManager { get; private set; }
    public static QueueObjectPool VFXPool { get; private set; }
    public static UnmanagedObjectPool BulletPool { get; private set; }

    public int targetFrameRate = 60;
    
    [SerializeField] private DatabaseManager databaseManager;
    [SerializeField] private ControllerFactory controllerFactory;
    [SerializeField] private ControllerManager controllerManager;
    [SerializeField] private QueueObjectPool vfxPool;
    [SerializeField] private UnmanagedObjectPool bulletPool;

    private void Awake()
    {
        if (Instance != null)
        {
            print("Game already exists, destroying.");
            Destroy(gameObject);
            return;
        }
        
        Instance = this;
        DontDestroyOnLoad(this);

        Application.targetFrameRate = targetFrameRate;

        DOTween.Init(true, true, LogBehaviour.Default)
            .SetCapacity(200, 10);
        
        EntityManager = new EntityManager();
        EntityFactory = new EntityFactory(databaseManager, new GridInventoryFactory());

        DatabaseManager = databaseManager;
        ControllerFactory = controllerFactory;
        ControllerManager = controllerManager;
        VFXPool = vfxPool;
        BulletPool = bulletPool;
    }
}