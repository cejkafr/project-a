﻿using Game.Frontend.Controllers;
using Photon.Realtime;

public interface IPlayerManager
{
    PPlayer LocalPlayer { get; }
    
    void Connected(Player player, bool master);
    void Disconnected();
    void PlayerConnected(Player player, bool master);
    void PlayerDisconnected(Player player, bool master);
}

public class PPlayer
{
    public readonly int ActorNumber;
    public readonly Player NetworkPlayer;
    public PlayerId Id;
    public SoldierController Controller;

    public PPlayer(Player networkPlayer)
    {
        ActorNumber = networkPlayer.ActorNumber;
        NetworkPlayer = networkPlayer;
    }
}