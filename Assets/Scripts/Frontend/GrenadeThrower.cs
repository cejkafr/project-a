﻿using System;
using System.Collections;
using Pools;
using UnityEngine;

public class GrenadeThrower : MonoBehaviour
{
    public QueueObjectPool pool;
    public GameObject grenadePrefab;
    public Transform gunOut;
    public float throwSpeed;
    public float force;

    private void OnEnable()
    {
        StartCoroutine(ThrowGrenadeCoroutine());
    }

    private IEnumerator ThrowGrenadeCoroutine()
    {
        var wfs = new WaitForSeconds(1 / throwSpeed);
        while (gameObject.activeInHierarchy)
        {
            yield return wfs;
            ThrowGrenade();
        }
    }

    private void ThrowGrenade()
    {
        var grenade = pool.CreateFromPool(grenadePrefab, gunOut.position, Quaternion.identity);
        var grb = grenade.GetComponent<Rigidbody>();
        grb.AddForce(gunOut.forward * force, ForceMode.Force);
    }
}