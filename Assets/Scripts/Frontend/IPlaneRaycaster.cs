﻿using UnityEngine;

namespace Frontend
{
    public readonly struct RaycastCursorTarget
    {
        public static readonly RaycastCursorTarget None = new RaycastCursorTarget(
            false, Vector3.zero, Vector3.zero, null);

        public static RaycastCursorTarget Hit(RaycastHit hit)
        {
            return new RaycastCursorTarget(true, hit.point, hit.normal, hit.transform.gameObject);
        }
        
        public static RaycastCursorTarget Hit(Vector3 point)
        {
            return new RaycastCursorTarget(false, point, Vector3.up, null);
        }

        public readonly bool Success;
        public readonly Vector3 Point;
        public readonly Vector3 Normal;
        public readonly GameObject GameObject;

        private RaycastCursorTarget(bool success, Vector3 point, Vector3 normal, GameObject gameObject)
        {
            Success = success;
            Point = point;
            Normal = normal;
            GameObject = gameObject;
        }
    }

    public interface IPlaneRaycaster
    {
        Vector3 CursorPosition(Vector2 mousePosition);
        RaycastCursorTarget RaycastCursor(Vector2 mousePosition);
        RaycastCursorTarget RaycastCursorWFallback(Vector2 mousePosition);
    }
}