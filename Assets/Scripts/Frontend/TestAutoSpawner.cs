﻿using System;
using Photon.Pun;

namespace Frontend
{
    public class TestAutoSpawner : MonoBehaviourPunCallbacks
    {
        private void Start()
        {
            if (PhotonNetwork.IsConnected && PhotonNetwork.IsMasterClient)
            {
                photonView.TransferOwnership(0);
            }
        }

        private void OnConnectedToServer()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.Instantiate(gameObject.name, transform.position, transform.rotation);
                Destroy(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
