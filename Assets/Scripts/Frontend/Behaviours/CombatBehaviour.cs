﻿using System.Collections;
using Game.Frontend.Controllers;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class CombatBehaviour : MonoBehaviour
    {
        #region Events

        public delegate void AimEvent();

        public event AimEvent OnAimStart;
        public event AimEvent OnAimEnd;
        public event WeaponController.WeaponEvent OnShootStart;
        public event WeaponController.WeaponEvent OnShootFinish;
        public event WeaponController.WeaponShootEvent OnShoot;
        public event WeaponController.WeaponEvent OnReloadStart;
        public event WeaponController.WeaponEvent OnReloadFinish;
        public event WeaponController.WeaponEvent OnReload;
        public event WeaponController.WeaponEvent OnEmpty;

        #endregion

        public float speed;
        private Animator animator;

        public bool Disabled { get; private set; }
        public bool TriggerInput { get; private set; }
        public bool AimInput { get; private set; }
        public WeaponController Weapon { get; private set; }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void SetEnabled(bool value)
        {
            Disabled = !value;
            RefreshInputs();
        }

        private void RefreshInputs()
        {
            if (Disabled || Weapon == null)
            {
                SetAim(false, speed);
                SetTrigger(false);
            }
            else
            {
                SetAim(AimInput, speed);
                SetTrigger(TriggerInput);
            }
        }

        #region Equip

        public void SetWeapon(WeaponController weapon)
        {
            if (Weapon != null)
            {
                Weapon.SetTrigger(false);
                UnBindWeapon();
            }

            Weapon = weapon;
            if (Weapon != null)
            {
                BindWeapon();
                RefreshInputs();
            }
        }

        private void BindWeapon()
        {
            Weapon.OnShootStart += OnShootStart;
            Weapon.OnShootFinish += OnShootFinish;
            Weapon.OnShoot += OnShoot;
            Weapon.OnReloadStart += OnReloadStart;
            Weapon.OnReloadFinish += OnReloadFinish;
            Weapon.OnReload += OnReload;
            Weapon.OnEmpty += OnEmpty;
            Weapon.OnShoot += ShootAnim;
            Weapon.OnReload += ReloadAnim;
        }

        private void UnBindWeapon()
        {
            Weapon.OnShoot -= ShootAnim;
            Weapon.OnReload -= ReloadAnim;
            Weapon.OnShootStart -= OnShootStart;
            Weapon.OnShootFinish -= OnShootFinish;
            Weapon.OnShoot -= OnShoot;
            Weapon.OnReloadStart -= OnReloadStart;
            Weapon.OnReloadFinish -= OnReloadFinish;
            Weapon.OnReload -= OnReload;
            Weapon.OnEmpty -= OnEmpty;
        }

        #endregion

        #region Shoot

        public void SetTrigger(bool pressed)
        {
            TriggerInput = pressed;
            if (Disabled || Weapon == null)
            {
                return;
            }

            Weapon.SetTrigger(pressed);
        }

        public void ShootAnim(Vector3 pos, Vector3 dir, bool fullAuto)
        {
            animator.SetTrigger(fullAuto ? SoldierAnimator.ShootContinuous : SoldierAnimator.Shoot);
        }

        #endregion

        #region Reload

        public bool Reload()
        {
            if (Disabled || Weapon == null)
            {
                return false;
            }

            return Weapon.Reload();
        }

        public void ReloadAnim()
        {
            animator.SetFloat(SoldierAnimator.EquipSpeed, Weapon.ReloadTimeMult);
            animator.SetTrigger(SoldierAnimator.Reload);
        }

        #endregion

        #region Aim

        public bool aiming;
        private Coroutine coroutine;

        public bool SetAim(bool value, float speed)
        {
            AimInput = value;

            if ((value && Disabled) || aiming == value)
            {
                return false;
            }

            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }

            this.speed = speed;
            coroutine = StartCoroutine(value ? _aim() : _atEase());
            return true;
        }

        private IEnumerator _aim()
        {
            aiming = true;

            var value = animator.GetFloat(SoldierAnimator.Aim);
            while (value <= 1f)
            {
                value += speed * Time.deltaTime;
                animator.SetFloat(SoldierAnimator.Aim, value);
                yield return null;
            }

            animator.SetFloat(SoldierAnimator.Aim, 1f);
            OnAimStart?.Invoke();
        }

        private IEnumerator _atEase()
        {
            aiming = false;

            var value = animator.GetFloat(SoldierAnimator.Aim);
            while (value >= 0f)
            {
                value -= speed * Time.deltaTime;
                animator.SetFloat(SoldierAnimator.Aim, value);
                yield return null;
            }

            animator.SetFloat(SoldierAnimator.Aim, 0f);
            OnAimEnd?.Invoke();
        }

        #endregion
    }
}