﻿using Cinemachine;
using Extensions;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class RotationBehaviour : MonoBehaviour
    {
        public bool disabled;
        public float speed;

        private Transform thirdPersonCamera;
        private CharacterController characterCtrl;
        private Animator animator;
        private bool aim;
        private Vector3 targetDir;
        private Vector3 smoothDir;

        private float singleStep;

        // aim
        [SerializeField] private Vector3 cursorPos;
        [SerializeField] private bool isRotating;
        [SerializeField] private float rotationAngle;

        private void Awake()
        {
            characterCtrl = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
            thirdPersonCamera = FindObjectOfType<Camera>().transform;
        }

        private void Update()
        {
            if (aim)
            {
                AimRotate();
            }
            else
            {
                Rotate();
            }
        }

        public void AimRotate(Vector3 pos)
        {
            if (disabled)
            {
                return;
            }

            aim = true;
            cursorPos = pos;
            targetDir = pos.Flatten() - transform.position.Flatten();
        }

        public void Rotate(Vector3 dir)
        {
            if (disabled)
            {
                return;
            }

            aim = false;
            targetDir = dir;
        }

        private void Rotate()
        {
            animator.SetFloat(SoldierAnimator.HorAimAngle,
                characterCtrl.velocity.magnitude > 0 ? CalculateAngle() : 0f);
            DoRotation();
        }

        private float CalculateAngle()
        {
            var va = transform.forward.Flatten();
            var vb = targetDir.Flatten();
            var angleRad = Mathf.Acos(Vector3.Dot(va, vb));
            var cross = Vector3.Cross(va, vb);
            if (Vector3.Dot(Vector3.up, cross) < 0)
            {
                angleRad = -angleRad;
            }

            return angleRad * Mathf.Rad2Deg;
        }

        private void AimRotate()
        {
            var angle = transform.FacingSignedAngleFlat(cursorPos);

            // if (cursorTarget.Success)
            // {
            //     var hAngle = Vector3.SignedAngle(weaponCtrl.gunOut.forward,
            //         cursorTarget.Point - weaponCtrl.gunOut.position, weaponCtrl.gunOut.right);
            //     animator.SetFloat(SoldierAnimator.VerAimAngle, hAngle, 0.15f, Time.deltaTime);
            // }

            rotationAngle += angle * speed * Time.deltaTime;

            if (angle > 0)
            {
                angle = Mathf.Min(angle, 60);
                rotationAngle = Mathf.Min(angle, rotationAngle);
            }

            if (angle < 0)
            {
                angle = Mathf.Max(angle, -60);
                rotationAngle = Mathf.Max(angle, rotationAngle);
            }

            if (angle == 0) rotationAngle = 0;

            animator.SetFloat(SoldierAnimator.HorAimAngle, angle, 0.15f, Time.deltaTime);
            if (isRotating || Mathf.Abs(rotationAngle) >= 10)
            {
                isRotating = Mathf.Abs(rotationAngle) >= 5;
                DoRotation();
            }
        }

        private void DoRotation()
        {
            singleStep = speed * Time.deltaTime;
            smoothDir = Vector3.RotateTowards(transform.forward, targetDir, singleStep, 0.0f);
            transform.rotation = Quaternion.LookRotation(smoothDir);
        }
    }
}