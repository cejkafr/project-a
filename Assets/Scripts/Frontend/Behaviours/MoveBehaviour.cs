﻿using FMODUnity;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class MoveBehaviour : MonoBehaviour
    {
        public delegate void MoveEvent();

        public event MoveEvent OnMoveStart;
        public event MoveEvent OnMoveFinish;
        public event MoveEvent OnFallStart;
        public event MoveEvent OnFallFinish;

        public bool disabled;
        public float acceleration;
        public float speed;
        public bool aim;

        private Animator animator;
        private CharacterController characterCtrl;
        [SerializeField] private Vector3 inputDir;
        private Vector3 inverseInputDir;
        [SerializeField] private float inputMag;
        private Vector3 velocity;
        private float horizontal;
        private float vertical;
        [SerializeField] private float currSpeed;
        private bool isGrounded = true;
        private bool isPrevGrounded = true;
        private float lastGroundedY;
        private EnvEventInstance eventInstance;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            characterCtrl = GetComponent<CharacterController>();

            eventInstance = MaterialFxManager.Instance.CreateFootstepEventInstance();
        }

        private void OnDestroy()
        {
            eventInstance.Release();
        }

        private void Update()
        {
            GroundLogic();
            HandleAcceleration();

            velocity = Physics.gravity;

            if (!disabled)
            {
                if (aim)
                {
                    velocity += inputDir * currSpeed;
                    inverseInputDir = transform.InverseTransformDirection(inputDir);
                    vertical = inverseInputDir.x;
                    horizontal = inverseInputDir.z;
                }
                else
                {
                    velocity += transform.forward * currSpeed;
                    horizontal = characterCtrl.velocity.magnitude;
                    vertical = 0f;
                }
            }

            animator.SetFloat(SoldierAnimator.Vertical, vertical);
            animator.SetFloat(SoldierAnimator.Horizontal, horizontal);
            animator.SetFloat(SoldierAnimator.InputMagnitude, inputMag * 2f, 0.2f, Time.deltaTime);

            characterCtrl.Move(velocity * Time.deltaTime);
        }

        public void Move(Vector3 dir)
        {
            inputDir = dir;
            inputMag = dir.magnitude;
        }

        private void GroundLogic()
        {
            isGrounded = characterCtrl.isGrounded;
            animator.SetBool(SoldierAnimator.Grounded, isGrounded);
            if (!isGrounded && isPrevGrounded)
            {
                OnFallStart?.Invoke();
                lastGroundedY = transform.position.y;
                if (!Physics.Raycast(transform.position, Vector3.down, 1.9f))
                {
                    animator.SetTrigger(SoldierAnimator.Fall);
                }
            }
            else if (isGrounded && !isPrevGrounded)
            {
                var fallHeight = lastGroundedY - transform.position.y;
                if (fallHeight > 2)
                {
                    animator.SetInteger(SoldierAnimator.LandMode, 1);
                }
                else
                {
                    animator.SetInteger(SoldierAnimator.LandMode, 0);
                }

                if (fallHeight > 0.3f)
                    animator.SetTrigger(SoldierAnimator.Land);
                OnFallFinish?.Invoke();
            }
            
            isPrevGrounded = isGrounded;
        }
        
        private void HandleAcceleration()
        {
            if (inputMag > 0)
            {
                currSpeed += acceleration * Time.deltaTime;
                currSpeed = Mathf.Min(speed, currSpeed);
            }
            else
            {
                currSpeed -= acceleration * Time.deltaTime;
                currSpeed = Mathf.Max(0, currSpeed);
            }
        }
        
        public void FootstepAnimationEvent(AnimationEvent ev)
        {
            if (ev.animatorClipInfo.weight < 0.5)
            {
                return;
            }

            animator.SetBool(SoldierAnimator.IsRightLegUp, ev.intParameter < 1);

            eventInstance.SetPosition(transform.position);
            MaterialFxManager.Instance.SetEnvSFX(null, ref eventInstance);
            eventInstance.SetVolume(speed > 3f ? 1f : 0.1f);
            eventInstance.Start();
        }
    }
}