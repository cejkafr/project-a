﻿using System.Collections;
using Combat;
using Databases;
using FMODUnity;
using Game.Frontend.Controllers;
using Photon.Pun;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class MeleeBehaviour : MonoBehaviour
    {
        public delegate void MeleeAttackEvent();

        public delegate void MeleeAttackHitEvent(GameObject target);

        public event MeleeAttackEvent OnStart;
        public event MeleeAttackEvent OnFinish;
        public event MeleeAttackHitEvent OnHit;

        public bool disabled;
        public bool attacking;
        public bool debug;

        [SerializeField] private Transform punchHand;
        [SerializeField] private Transform kickFoot;
        [SerializeField] private string throwableTag;
        [EventRef] [SerializeField] private string hitSfx;

        private CharacterController characterCtrl;
        private Animator animator;
        private float cTime;
        private float cCurveMult;
        private Vector3 cDir;
        private string attackFx;

        public MeleeAttack meleeAttack;

        private void Awake()
        {
            characterCtrl = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
        }

        public void Init(string attackFx)
        {
            this.attackFx = attackFx;
        }

        public bool Attack(MeleeAttack attack)
        {
            if (disabled || attacking)
            {
                return false;
            }

            meleeAttack = attack;
            StartCoroutine(_Attack());
            return true;
        }

        public void AttackFX(MeleeAttack attack)
        {
            animator.SetInteger(SoldierAnimator.MeleeType, attack.animationId);
            animator.SetTrigger(SoldierAnimator.Melee);
        }

        private IEnumerator _Attack()
        {
            attacking = true;
            OnStart?.Invoke();

            RuntimeManager.PlayOneShot(attackFx, transform.position);

            var damageApplied = false;

            cTime = 0f;
            cCurveMult = 1 / meleeAttack.length;

            AttackFX(meleeAttack);

            while (cTime < meleeAttack.length)
            {
                cTime += Time.deltaTime;

                Move();
                if (!damageApplied && cTime > meleeAttack.applyDamageDelay)
                {
                    ApplyDamage();
                    damageApplied = true;
                }

                yield return null;
            }

            attacking = false;
            OnFinish?.Invoke();
            meleeAttack = null;
        }

        private void Move()
        {
            cDir = transform.forward * meleeAttack.movementCurve.Evaluate(cTime * cCurveMult);
            cDir += Physics.gravity;
            characterCtrl.Move(cDir * Time.deltaTime);
        }

        private void ApplyDamage()
        {
            var cTransform = transform;
            var cForward = cTransform.forward;

            var start = cTransform.position + Vector3.up + cForward * meleeAttack.raycastOffset.x;
            var end = start + cForward * meleeAttack.raycastOffset.y;

            var targets = Physics.OverlapCapsule(
                cTransform.position + Vector3.up + cForward * (meleeAttack.raycastOffset.x * 0.5f), end,
                meleeAttack.raycastOffset.z * 2f);
            // Draw.ingame.WireCapsule(
            //     cTransform.position + Vector3.up + cForward * (meleeAttack.raycastOffset.x * 0.5f), end,
            //     meleeAttack.raycastOffset.z * 2f, Color.red);

            // if (targets.Length > 0)
            // {
            //     RuntimeManager.PlayOneShot(
            //         meleeAttack.type == MeleeAttack.Type.Hard
            //             ? heavySfx
            //             : normalSfx, transform.position);
            // }
            
            foreach (var target in targets)
            {
                var rb = target.GetComponent<Rigidbody>();
                if (rb == null) continue;
                rb.AddForce(rb.velocity * -1 * 100f, ForceMode.Force);
            }

            targets = Physics.OverlapCapsule(start, end, meleeAttack.raycastOffset.z);
            // Draw.ingame.WireCapsule(start, end, meleeAttack.raycastOffset.z);
            foreach (var target in targets)
            {
                if (target.gameObject.CompareTag("Throwable")) continue;
                var health = target.GetComponent<IHasHealth>();
                health?.ApplyDamage(meleeAttack.damage);
                var rb = target.GetComponent<Rigidbody>();
                var view = target.GetComponent<PhotonView>();
                if (view != null) view.TransferOwnership(GetComponent<SoldierController>().photonView.Owner);
                if (rb == null) continue;
                rb.AddForceAtPosition(cForward * meleeAttack.rbForce, target.ClosestPoint(end), ForceMode.Force);
            }
        }
    }
}