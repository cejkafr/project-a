﻿using System.Collections;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class ClimbBehaviour : MonoBehaviour
    {
        public delegate void ClimbEvent();

        public event ClimbEvent OnStart;
        public event ClimbEvent OnFinish;

        public bool disabled;

        private CharacterController characterCtrl;
        private Animator animator;
        
        private void Awake()
        {
            characterCtrl = GetComponent<CharacterController>();
            animator = GetComponent<Animator>();
        }
        
        public bool Climb()
        {
            if (disabled)
            {
                return false;
            }
            
            var ray = new Ray(transform.position + Vector3.up, transform.forward);
            if (Physics.Raycast(ray, out var hit, 1))
            {
                var c = hit.collider;
                if (c == null) return false;
                if (c.bounds.size.y > 1) return false;

                // canMove = false;
                // canRotate = false;
                // animator.SetTrigger(Climb);
                // transform.DOMoveY(transform.position.y + 1.5f, 0.8f);
                // transform.DOMove(transform.position + (transform.forward * .3f), 0.3f).SetDelay(0.6f)
                //     .onComplete += () =>
                // {
                //     canMove = true;
                //     canRotate = true;
                // };

                StartCoroutine(_Climb(1));
            }

            return true;
        }

        private IEnumerator _Climb(int height)
        {
            print("climbing");

            OnStart?.Invoke();

            animator.SetTrigger(SoldierAnimator.Climb);

            characterCtrl.enabled = false;

            var delta = 0f;
            while (delta < height)
            {
                var vector = Vector3.up * 2f * Time.deltaTime;
                delta += vector.y;
                transform.Translate(vector);
                yield return new WaitForEndOfFrame();
                print(delta);
            }

            delta = 0f;
            while (delta < 0.4)
            {
                var vector = Vector3.forward * 3f * Time.deltaTime;
                delta += vector.magnitude;
                transform.Translate(vector);
                yield return new WaitForEndOfFrame();
            }

            characterCtrl.enabled = true;

            yield return new WaitForSeconds(0.4f);

            OnFinish?.Invoke();

            print("climb end");
        }
    }
}