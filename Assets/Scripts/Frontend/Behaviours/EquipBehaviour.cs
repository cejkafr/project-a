﻿using System;
using System.Collections;
using Game.Frontend.Controllers;
using UnityEngine;

namespace Frontend.Behaviours
{
    public class EquipBehaviour : MonoBehaviour
    {
        public delegate void EquipItemEvent();
        
        public event EquipItemEvent OnStart;
        public event EquipItemEvent OnFinish;

        public bool disabled;
        public Transform holdingHand;
        public ItemBodySlot[] itemBodySlots;

        private Animator animator;
        private Coroutine equipCoroutine;
        private Coroutine unEquipCoroutine;

        public WeaponController item;

        public bool IsHolstered => item == null;
        public bool IsEquipped => item != null;
        public bool InProgress => IsUnEquipping || IsEquipping;
        public bool IsUnEquipping { get; private set; }
        public bool IsEquipping { get; private set; }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public bool UnEquip(WeaponController weapon, Action onComplete = null)
        {
            if (disabled || (IsHolstered && !IsEquipping))
            {
                return false;
            }
            unEquipCoroutine = StartCoroutine(_UnEquip(weapon, onComplete));
            return true;
        }

        public bool Equip(WeaponController weapon, Action onComplete = null)
        {
            if (disabled || (!IsHolstered && !IsUnEquipping))
            {
                return false;
            }
            equipCoroutine = StartCoroutine(_Equip(weapon, onComplete));
            return true;
        }

        private IEnumerator _Equip(WeaponController weapon, Action onComplete)
        {
            if (unEquipCoroutine != null)
                yield return unEquipCoroutine;

            IsEquipping = true;
            IsUnEquipping = false;
            OnStart?.Invoke();

            animator.SetInteger(SoldierAnimator.MotionId, weapon.Motion.id);
            animator.SetFloat(SoldierAnimator.EquipSpeed, weapon.Motion.equipUnEquipSpeed);
            animator.SetTrigger(SoldierAnimator.EquipWeapon);

            yield return new WaitForSeconds(weapon.Motion.equipShowDelay / weapon.Motion.equipUnEquipSpeed);

            weapon.EquipInHand(holdingHand);

            yield return new WaitForSeconds(
                (weapon.Motion.equipTimeTotal - weapon.Motion.equipShowDelay) / weapon.Motion.equipUnEquipSpeed);

            item = weapon;
            IsEquipping = false;
            equipCoroutine = null;
            
            onComplete?.Invoke();
            OnFinish?.Invoke();
        }

        private IEnumerator _UnEquip(WeaponController weapon, Action onComplete)
        {
            if (equipCoroutine != null)
                yield return equipCoroutine;

            IsEquipping = false;
            IsUnEquipping = true;
            OnStart?.Invoke();

            item = null;

            animator.SetFloat(SoldierAnimator.EquipSpeed, weapon.Motion.equipUnEquipSpeed);
            animator.SetTrigger(SoldierAnimator.UnEquipWeapon);

            yield return new WaitForSeconds(weapon.Motion.unEquipHideDelay / weapon.Motion.equipUnEquipSpeed);

            weapon.EquipIn(itemBodySlots);

            yield return new WaitForSeconds(
                (weapon.Motion.unEquipTimeTotal - weapon.Motion.unEquipHideDelay) / weapon.Motion.equipUnEquipSpeed);

            unEquipCoroutine = null;
            
            IsUnEquipping = false;
            onComplete?.Invoke();
            OnFinish?.Invoke();
        }
    }
}