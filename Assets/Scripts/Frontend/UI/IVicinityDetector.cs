﻿using System.Collections.Generic;
using Game.Entity;
using UnityEngine;

namespace Frontend.UI
{
    public interface IVicinityDetector
    {
        void DetectLootBoxes(Vector3 position, List<LootBox> results);
    }
}