﻿using System.Collections.Generic;
using Extensions;
using Game.Networking;
using UnityEngine;

namespace Frontend.UI
{
    public class PlayerListUI : MonoBehaviour
    {
        private readonly List<PlayerRowUI> usedRows = new List<PlayerRowUI>(5);
        private readonly Queue<PlayerRowUI> freeRows = new Queue<PlayerRowUI>(5);

        [SerializeField] private GameObject rowPrefab;
        [SerializeField] private Transform content;

        private void Start()
        {
            InvokeRepeating(nameof(Refresh), 1, 2);
        }

        public void Refresh()
        {
            FreeAllRows();
            
            foreach (var player in MultiplayerGame.PlayerManager.Players)
            {
                if (player == null)
                {
                    continue;
                }
                var row = GetFreeRow();
                row.SetValue(player);
                row.SetActive(true);
            }
        }

        private void FreeAllRows()
        {
            foreach (var row in usedRows)
            {
                row.SetActive(false);
                freeRows.Enqueue(row);
            }
            usedRows.Clear();
        }
        
        private PlayerRowUI GetFreeRow()
        {
            if (freeRows.IsEmpty())
            {
                var inst = Instantiate(rowPrefab, content);
                freeRows.Enqueue(inst.GetComponent<PlayerRowUI>());
            }
            var row = freeRows.Dequeue();
            usedRows.Add(row);
            return row;
        }
    }
}
