﻿using System;
using Core.Frontend;
using Frontend.UI.Players;
using UnityEngine;
using UnityEngine.Events;

namespace Frontend.UI
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private PlayerUI playerHud;
        [SerializeField] private PlayerInventoryUI playerInventory;

        public UnityEvent<bool> onEnable;
        
        public PlayerInventoryUI PlayerInventory => playerInventory;

        private void Start()
        {
            playerInventory.SetOpen(false);
        }

        public void ShowHUD(bool value)
        {
            playerHud.SetActive(value);
            onEnable.Invoke(!value);
        }

        public void OpenInventory(bool value)
        {
            playerInventory.SetOpen(value);
            TooltipSystem.Hide();
            onEnable.Invoke(value);
        }

        public void CloseAllWindows()
        {
            playerInventory.SetOpen(false);
            onEnable.Invoke(false);
        }
    }
}