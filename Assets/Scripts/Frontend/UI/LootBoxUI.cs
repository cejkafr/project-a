﻿using Game.Entity;
using Game.Inventory.UI;
using TMPro;
using UnityEngine;

namespace Frontend.UI
{
    public class LootBoxUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI header;
        [SerializeField] private GridInventoryUI inventory;

        public void BindTo(LootBox lootBox)
        {
            header.text = lootBox.Entry.title;
            inventory.BindTo(lootBox.Inventory);
        }

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}