﻿using System.Collections.Generic;
using Extensions;
using Game.Entity;
using UnityEngine;

namespace Frontend.UI
{
    public class VicinityUI : MonoBehaviour
    {
        [SerializeField] private GameObject lootBoxPrefab;
        [SerializeField] private Transform content;

        public IVicinityDetector vicinityDetector;

        private Transform playerTransform;
        private readonly List<LootBox> lootBoxes = new List<LootBox>();
        private readonly List<LootBoxUI> used = new List<LootBoxUI>();
        private readonly Queue<LootBoxUI> free = new Queue<LootBoxUI>();

        private void Awake()
        {
            vicinityDetector = GetComponent<IVicinityDetector>();
        }

        public void BindTo(Transform playerTransform)
        {
            this.playerTransform = playerTransform;
        }

        public void Refresh()
        {
            FreeAll();
            if (playerTransform == null)
                return;
            vicinityDetector.DetectLootBoxes(playerTransform.position, lootBoxes);
            foreach (var lootBox in lootBoxes)
            {
                var lootBoxUi = GetOrCreateLootBox();
                lootBoxUi.SetActive(true);
                lootBoxUi.BindTo(lootBox);
            }
        }

        private void FreeAll()
        {
            var tmp = used.ToArray();
            foreach (var lootBox in tmp)
            {
                used.Remove(lootBox);
                free.Enqueue(lootBox);
                lootBox.SetActive(false);
            }
        }

        private LootBoxUI GetOrCreateLootBox()
        {
            if (free.IsEmpty())
            {
                var inst = Instantiate(lootBoxPrefab, content);
                var lootBox = inst.GetComponent<LootBoxUI>();
                free.Enqueue(lootBox);
            }

            var dequeued = free.Dequeue();
            used.Add(dequeued);
            return dequeued;
        }
    }
}