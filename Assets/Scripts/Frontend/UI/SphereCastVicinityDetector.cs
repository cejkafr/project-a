﻿using System.Collections.Generic;
using Game.Entity;
using Game.Frontend.Controllers;
using UnityEngine;

namespace Frontend.UI
{
    public class SphereCastVicinityDetector : MonoBehaviour, IVicinityDetector
    {
        [SerializeField] private LayerMask lootBoxLayer;
        [SerializeField] private LayerMask itemLayer;
        [SerializeField] private float radius;
        [SerializeField] private int maxResults = 4;

        private Collider[] results;

        private void Awake()
        {
            results = new Collider[maxResults];
        }

        public void DetectLootBoxes(Vector3 position, List<LootBox> results)
        {
            results.Clear();
            var count = Physics.OverlapSphereNonAlloc(position, radius, this.results, lootBoxLayer);
            for (var i = 0; i < count; i++)
            {
                var controller = this.results[i].GetComponent<LootBoxController>();
                if (controller == null)
                    continue;
                results.Add(controller.Entity);
            }
        }
    }
}