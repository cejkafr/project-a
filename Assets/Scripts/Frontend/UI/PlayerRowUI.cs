﻿using Game.Entity;
using TMPro;
using UnityEngine;

namespace Frontend.UI
{
    public class PlayerRowUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI id;
        [SerializeField] private TextMeshProUGUI name;
        [SerializeField] private TextMeshProUGUI actorNumber;
        [SerializeField] private TextMeshProUGUI controllerViewId;

        public void SetValue(PPlayer player)
        {
            id.text = player.Id.ToString();
            name.text = player.NetworkPlayer.NickName;
            actorNumber.text = player.ActorNumber.ToString();
            controllerViewId.text = player.Controller != null ?  player.Controller.photonView.ViewID.ToString() : "none";
        }
        
        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}
