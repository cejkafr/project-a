﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace Frontend.UI
{
    public class PlayerInputField : MonoBehaviour
    {
        private const string PlayerNamePrefKey = "PlayerName";

        // Start is called before the first frame update
        private void Start()
        {
            var defaultName = string.Empty;
            var inputField = GetComponent<InputField>();
            if (inputField != null)
            {
                if (PlayerPrefs.HasKey(PlayerNamePrefKey))
                {
                    defaultName = PlayerPrefs.GetString(PlayerNamePrefKey);
                    inputField.text = defaultName;
                }
            }


            PhotonNetwork.NickName = defaultName;
        }

        public void SetPlayerName(string value)
        {
            // #Important
            if (string.IsNullOrEmpty(value))
            {
                Debug.LogError("Player Name is null or empty");
                return;
            }

            PhotonNetwork.NickName = value;


            PlayerPrefs.SetString(PlayerNamePrefKey, value);
        }
    }
}