﻿using System.Globalization;
using Game.Frontend.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frontend.UI.Players
{
    public class PlayerUI : MonoBehaviour
    {
        public Image portrait;
        public Slider health;
        public TextMeshProUGUI healthText;
        public TextMeshProUGUI healthMaxText;
        public Slider stamina;
        public TextMeshProUGUI staminaText;
        public TextMeshProUGUI staminaMaxText;
        
        private SoldierController player;

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
        
        public void SetPlayer(SoldierController player)
        {
            this.player = player;
            portrait.sprite = player.Entity.Entry.icon;

            HealthChangeHandler();
            StaminaChangeHandler();

            player.OnHealthChanged += HealthChangeHandler;
            player.OnStaminaChanged += StaminaChangeHandler;
        }

        public void HealthChangeHandler()
        {
            health.maxValue = player.Entity.StaminaMax;
            health.value = player.Entity.Health;
            healthText.text = Mathf.RoundToInt(player.Entity.Health).ToString(CultureInfo.CurrentCulture);
            healthMaxText.text = player.Entity.HealthMax.ToString(CultureInfo.CurrentCulture);
        }
        
        public void StaminaChangeHandler()
        {
            stamina.maxValue = player.Entity.StaminaMax;
            stamina.value = player.Entity.Stamina;
            staminaText.text =  Mathf.RoundToInt(player.Entity.Stamina).ToString(CultureInfo.CurrentCulture);
            staminaMaxText.text = player.Entity.StaminaMax.ToString(CultureInfo.CurrentCulture);
        }
        
    }
}