﻿using Game.Frontend.Controllers;
using UnityEngine;

namespace Frontend.UI.Players
{
    public class PlayerHud : MonoBehaviour
    {
        [SerializeField] private PlayerUI player;
        [SerializeField] private WeaponUI weapon;
        [SerializeField] private PartyUI party;

        public void SetPlayer(SoldierController controller)
        {
            player.SetPlayer(controller);
            weapon.SetPlayer(controller);
        }
        
        public void SetOpen(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}