﻿using Game.Frontend.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frontend.UI.Players
{
    public class WeaponUI : MonoBehaviour
    {
        public TextMeshProUGUI weaponName;
        public Slider ammo;
        public TextMeshProUGUI ammoText;
        public TextMeshProUGUI maxAmmoText;
        
        private SoldierController player;

        public void SetPlayer(SoldierController player)
        {
            this.player = player;
            
            WeaponChangeHandler();

            player.EquipBehaviour.OnFinish += WeaponChangeHandler;
            player.CombatBehaviour.OnReloadFinish += AmmoChangeHandler;
            player.CombatBehaviour.OnShoot += AmmoChangeHandler;
        }

        public void AmmoChangeHandler(Vector3 pos, Vector3 dir, bool continuous)
        {
            AmmoChangeHandler();
        }

        public void AmmoChangeHandler()
        {
            ammo.maxValue = player.CombatBehaviour.Weapon.Entity.AmmoMax;
            ammo.value = player.CombatBehaviour.Weapon.Entity.Ammo;
            ammoText.text = player.CombatBehaviour.Weapon.Entity.Ammo.ToString();
            maxAmmoText.text = player.CombatBehaviour.Weapon.Entity.AmmoMax.ToString();
        }

        public void WeaponChangeHandler()
        {
            if (player.CombatBehaviour.Weapon == null)
            {
                weaponName.gameObject.SetActive(false);
                ammo.gameObject.SetActive(false);
            }
            else
            {
                weaponName.gameObject.SetActive(true);
                ammo.gameObject.SetActive(true);
                weaponName.text = player.CombatBehaviour.Weapon.Entity.Title;
                AmmoChangeHandler();
            }
        }
    }
}