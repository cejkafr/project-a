﻿using Game.Entity;
using Game.Frontend.Controllers;
using Game.Inventory;
using Game.Inventory.UI;
using UnityEngine;

namespace Frontend.UI
{
    public class PlayerInventoryUI : MonoBehaviour
    {
        public static int SlotSize;
        public static int SlotSpacing;
        
        [SerializeField] private EquipmentUI equipment;
        [SerializeField] private GridInventoryUI inventory;
        [SerializeField] private VicinityUI vicinity;

        public int slotSize;
        public int slotSpacing;

        private void Awake()
        {
            SlotSize = slotSize;
            SlotSpacing = slotSpacing;
        }

        public void SetPlayer(SoldierController controller)
        {
            SetEntity(controller.Entity, controller.transform);
        }

        public void SetEntity(ICombinedInventoryEntity entity, Transform vicinityTransform)
        {
            equipment.BindTo(entity.Equipment);
            inventory.BindTo(entity.Inventory);
            vicinity.BindTo(vicinityTransform);            
        }

        public void SetOpen(bool value)
        {
            gameObject.SetActive(value);
            if (value) 
                vicinity.Refresh();
        }
    }
}