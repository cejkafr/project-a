﻿using Game.Entity;
using TMPro;
using UnityEngine;

namespace Frontend.UI
{
    public class EntityRowUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI id;
        [SerializeField] private TextMeshProUGUI entryId;
        [SerializeField] private TextMeshProUGUI type;
        [SerializeField] private TextMeshProUGUI owner;

        public void SetValue(IEntity entity)
        {
            id.text = entity.Id.ToString();
            entryId.text = entity.EntryId;
            type.text = entity.GetType().Name;
            owner.text = entity.Owner.ToString();
        }
        
        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}
