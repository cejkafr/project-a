﻿using System.Collections.Generic;
using Extensions;
using UnityEngine;

namespace Frontend.UI
{
    public class EntityListUI : MonoBehaviour
    {
        private readonly List<EntityRowUI> usedRows = new List<EntityRowUI>(5);
        private readonly Queue<EntityRowUI> freeRows = new Queue<EntityRowUI>(5);

        [SerializeField] private GameObject rowPrefab;
        [SerializeField] private Transform content;

        private void Start()
        {
            InvokeRepeating(nameof(Refresh), 1, 2);
        }

        public void Refresh()
        {
            FreeAllRows();
            
            foreach (var keyPair in GameManager.EntityManager.Entities)
            {
                var row = GetFreeRow();
                row.SetValue(keyPair.Value);
                row.SetActive(true);
            }
        }

        private void FreeAllRows()
        {
            foreach (var row in usedRows)
            {
                row.SetActive(false);
                freeRows.Enqueue(row);
            }
            usedRows.Clear();
        }
        
        private EntityRowUI GetFreeRow()
        {
            if (freeRows.IsEmpty())
            {
                var inst = Instantiate(rowPrefab, content);
                freeRows.Enqueue(inst.GetComponent<EntityRowUI>());
            }
            var row = freeRows.Dequeue();
            usedRows.Add(row);
            return row;
        }
    }
}
