﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

namespace Game.Networking
{
    public class NetworkEventHandler : INetworkEventHandler, IOnEventCallback
    {
        public event Action<SpawnedEvent> OnSpawned;
        public event Action<SyncStateEvent> OnSyncState;
        public event Action<SpawnPlayer> OnSpawn;
        public event Action<EntityNetworkSyncEvent> OnSyncEntity;

        private Dictionary<byte, List<Action<object>>> handlers 
            = new Dictionary<byte, List<Action<object>>>();

        public void OnEvent(EventData ev)
        {
            switch (ev.Code)
            {
                case (byte) NetworkEventType.Spawned:
                    OnSpawned?.Invoke(new SpawnedEvent(ev.CustomData));
                    break;
                case (byte) NetworkEventType.SyncState:
                    OnSyncState?.Invoke(new SyncStateEvent(ev.CustomData));
                    break;
                case (byte) NetworkEventType.SpawnPlayer:
                    OnSpawn?.Invoke(new SpawnPlayer(ev.CustomData));
                    break;
                case (byte) NetworkEventType.SyncEntity:
                    OnSyncEntity?.Invoke(new EntityNetworkSyncEvent(ev.CustomData));
                    break;
            }

            if (handlers.ContainsKey(ev.Code))
            {
                foreach (var handler in handlers[ev.Code])
                {
                    handler.Invoke(ev.CustomData);
                }
            }
        }

        public void RegisterListener(byte type, Action<object> listener)
        {
            if (!handlers.ContainsKey(type))
            {
                handlers.Add(type, new List<Action<object>>(1));
            }
            
            handlers[type].Add(listener);
        }

        public void Send(INetworkEvent ev)
        {
            PhotonNetwork.RaiseEvent(ev.Code, ev.Serialize(),
                RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        public void Send(INetworkEvent ev, int actorNumber)
        {
            var opts = new RaiseEventOptions
            {
                CachingOption = EventCaching.DoNotCache,
                TargetActors = new[]
                {
                    actorNumber
                },
            };
            PhotonNetwork.RaiseEvent(ev.Code, ev.Serialize(), opts, SendOptions.SendReliable);
        }

        public void SendToMaster(INetworkEvent ev)
        {
            var opts = new RaiseEventOptions
            {
                CachingOption = EventCaching.DoNotCache,
                TargetActors = new[]
                {
                    PhotonNetwork.MasterClient.ActorNumber
                },
            };
            PhotonNetwork.RaiseEvent(ev.Code, ev.Serialize(), opts, SendOptions.SendReliable);
        }

        public void SendBuffered(INetworkEvent ev)
        {
            var opts = new RaiseEventOptions
            {
                CachingOption = EventCaching.AddToRoomCacheGlobal,
                Receivers = ReceiverGroup.Others,
            };
            PhotonNetwork.RaiseEvent(ev.Code, ev.Serialize(), opts, SendOptions.SendReliable);
        }
    }
}