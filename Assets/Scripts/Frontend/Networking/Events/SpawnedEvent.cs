﻿namespace Game.Networking
{
    public readonly struct SpawnedEvent : INetworkEvent
    {
        public static byte TypeCode => (byte) NetworkEventType.Spawned;
        
        public byte Code => TypeCode;

        public readonly PlayerId Id;
        public readonly int ActorNumber;
        public readonly int ControllerViewId;

        public SpawnedEvent(object data)
        {
            var arr = (object[]) data;
            Id = (PlayerId) arr[0];
            ActorNumber = (int) arr[1];
            ControllerViewId = (int) arr[2];
        }

        public SpawnedEvent(PPlayer player)
        {
            Id = player.Id;
            ActorNumber = player.NetworkPlayer.ActorNumber;
            ControllerViewId = player.Controller.photonView.ViewID;
        }

        public object Serialize()
        {
            return new object[]
            {
                Id,
                ActorNumber,
                ControllerViewId,
            };
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(ActorNumber)}: {ActorNumber}, {nameof(ControllerViewId)}: {ControllerViewId}, {nameof(Code)}: {Code}";
        }
    }
}