﻿using UnityEngine;

namespace Game.Networking
{
    public readonly struct SpawnPlayer : INetworkEvent
    {
        public static byte TypeCode => (byte) NetworkEventType.SpawnPlayer;

        public byte Code => TypeCode;

        public readonly int soldierId;
        public readonly Vector3 position;
        public readonly Quaternion rotation;

        public SpawnPlayer(int soldierId, Vector3 position, Quaternion rotation)
        {
            this.soldierId = soldierId;
            this.position = position;
            this.rotation = rotation;
        }

        public SpawnPlayer(object data)
        {
            var arr = (object[]) data;
            soldierId = (int) arr[0];
            position = (Vector3) arr[1];
            rotation = (Quaternion) arr[2];
        }

        public object Serialize()
        {
            return new object[]
            {
                soldierId,
                position,
                rotation,
            };
        }

        public override string ToString()
        {
            return $"{nameof(soldierId)}: {soldierId}, {nameof(position)}: {position}, {nameof(rotation)}: {rotation}, {nameof(Code)}: {Code}";
        }
    }
}