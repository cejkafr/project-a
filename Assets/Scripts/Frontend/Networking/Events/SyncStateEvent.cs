﻿namespace Game.Networking
{
    public readonly struct SyncStateEvent : INetworkEvent
    {
        public static byte TypeCode => (byte) NetworkEventType.SyncState;
        
        public byte Code => TypeCode;

        public readonly PlayerId Id;
        public readonly int ActorNumber;
        public readonly int ControllerViewId;
        public readonly EquipmentSlotType CurrentEquipment;
        public readonly bool IsArmed;
        public readonly bool IsAiming;

        public SyncStateEvent(object data)
        {
            var arr = (object[]) data;
            Id = (PlayerId) arr[0];
            ActorNumber = (int) arr[1];
            ControllerViewId = (int) arr[2];
            CurrentEquipment = (EquipmentSlotType) arr[3];
            IsArmed = (bool) arr[4];
            IsAiming = (bool) arr[5];
        }

        public SyncStateEvent(PPlayer player)
        {
            Id = player.Id;
            ActorNumber = player.NetworkPlayer.ActorNumber;
            if (player.Controller != null)
            {
                ControllerViewId = player.Controller.photonView.ViewID;
                CurrentEquipment = player.Controller.CurrentEquipment;
                IsArmed = player.Controller.IsArmed;
                IsAiming = player.Controller.Aiming;
            }
            else
            {
                ControllerViewId = -1;
                CurrentEquipment = EquipmentSlotType.None;
                IsArmed = false;
                IsAiming = false;
            }
        }

        public object Serialize()
        {
            return new object[]
            {
                Id,
                ActorNumber,
                ControllerViewId,
                CurrentEquipment, 
                IsArmed, 
                IsAiming
            };
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(ActorNumber)}: {ActorNumber}, {nameof(ControllerViewId)}: {ControllerViewId}, {nameof(CurrentEquipment)}: {CurrentEquipment}, {nameof(IsArmed)}: {IsArmed}, {nameof(IsAiming)}: {IsAiming}, {nameof(Code)}: {Code}";
        }
    }
}