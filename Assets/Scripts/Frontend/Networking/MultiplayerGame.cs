﻿using System.Collections.Generic;
using Frontend;
using Game.Entity;
using Game.Frontend.Controllers;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Game.Networking
{
    public class MultiplayerGame : MonoBehaviourPunCallbacks
    {
        public static MultiplayerGame Instance { get; private set; }
        public static NetworkPlayerManager PlayerManager { get; private set; }

        public string soldierId;
        public string[] weaponIds;
        public string[] grenadeIds;
        public string[] ammoIds;
        public string gameVersion = "0.0.1";
        public byte maxPlayersPerRoom = 4;

        public UnityEvent<SoldierController> onPlayerSpawned;

        private bool isConnecting;

        public NetworkEventHandler eventHandler;
        public EntityNetworkSync entityNetworkSync;
        private InventoryNetworkSync inventoryNetworkSync;
        private EquipmentNetworkSync equipmentNetworkSync;

        private void Awake()
        {
            if (Instance != null)
            {
                print("MultiplayerGame already exists, destroying.");
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }

        private void Start()
        {
            eventHandler = new NetworkEventHandler();
            eventHandler.OnSpawned += OnSpawned;
            eventHandler.OnSpawn += OnSpawn;
            inventoryNetworkSync = new InventoryNetworkSync(GameManager.EntityManager, eventHandler);
            equipmentNetworkSync = new EquipmentNetworkSync(GameManager.EntityManager, eventHandler);
            entityNetworkSync = new EntityNetworkSync(GameManager.EntityManager, GameManager.EntityFactory, 
                eventHandler, inventoryNetworkSync, equipmentNetworkSync);
            
            PlayerManager = new NetworkPlayerManager(eventHandler);
            PlayerManager.OnPlayerConnectedToMaster += OnPlayerConnectedToMaster;

            GameManager.EntityFactory.InventoryFactory = new ServerAuthorityInventoryFactory(GameManager.EntityManager, eventHandler);
            
            PhotonNetwork.AddCallbackTarget(eventHandler);
            SceneManager.sceneLoaded += OnSceneLoaded;
            PhotonNetwork.AutomaticallySyncScene = true;

            Connect();
        }

        public void Connect()
        {
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                isConnecting = PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = gameVersion;
            }
        }
        
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster() was called by PUN");

            if (isConnecting)
            {
                // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
                PhotonNetwork.JoinRandomRoom();
                isConnecting = false;
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            PhotonNetwork.CreateRoom("Test", new RoomOptions {MaxPlayers = maxPlayersPerRoom});
        }
        
        public override void OnDisconnected(DisconnectCause cause)
        {
            PlayerManager.Disconnected();
            GameManager.EntityManager.Clear();
        }

        public override void OnLeftRoom()
        {
            PlayerManager.Disconnected();
            GameManager.EntityManager.Clear();
            // SceneManager.LoadScene(0);
        }
        
        public override void OnJoinedRoom()
        {
            Logger.Info(Logger.Type.Networking, "Joined room: {0}", PhotonNetwork.CurrentRoom);

            PlayerManager.Connected(PhotonNetwork.LocalPlayer, PhotonNetwork.IsMasterClient);
            if (PhotonNetwork.IsMasterClient)
            {
                // var ammo = GameManager.EntityFactory.CreateAmmoBox("9mmBox10");
                // GameManager.EntityManager.Create(ammo);
                // var lb = GameManager.EntityFactory.CreateLootBox("boxSmall");
                // GameManager.EntityManager.Create(lb);
                // lb.Inventory.Insert(ammo);
                //
                // var data = new object[] { lb.Id };
                // var inst = PhotonNetwork.Instantiate(lb.Entry.prefab.name, new Vector3(3, 0, 4), Quaternion.identity, 0, data);
                
                for (var i = 1; i <= 4; i++)
                {
                    var s = GameManager.EntityFactory.CreateSoldier(soldierId, ownerId: (PlayerId) i);
                    GameManager.EntityManager.Create(s);
                    foreach (var weaponId in weaponIds)
                    {
                        var w = GameManager.EntityFactory.CreateWeapon(weaponId, ownerId: (PlayerId) i);
                        GameManager.EntityManager.Create(w);
                        s.Equipment.Equip(w);
                    }
                    foreach (var grenadeId in grenadeIds)
                    {
                        var w = GameManager.EntityFactory.CreateThrowable(grenadeId, ownerId: (PlayerId) i);
                        GameManager.EntityManager.Create(w);
                        s.Equipment.Equip(w);
                    }
                    foreach (var ammoId in ammoIds)
                    {
                        var w = GameManager.EntityFactory.CreateAmmoBox(ammoId, ownerId: (PlayerId) i);
                        GameManager.EntityManager.Create(w);
                        s.Equipment.Equip(w);
                    }
                }

                var playerSpawn = FindObjectOfType<PlayerSpawn>();
                var spawnTransform = playerSpawn.GetPoint(PlayerManager.LocalPlayer.Id);
                var soldier = GameManager.EntityManager.GetFirstByOwner<Soldier>(PlayerManager.LocalPlayer.Id);
                var equipment = GameManager.EntityManager.GetByOwner<Weapon>(PlayerManager.LocalPlayer.Id);

                Spawn(soldier, equipment, spawnTransform.position, spawnTransform.rotation);
            }
        }

        public override void OnPlayerEnteredRoom(Player other)
        {
            PlayerManager.PlayerConnected(other, PhotonNetwork.IsMasterClient);
        }

        public override void OnPlayerLeftRoom(Player other)
        {
            PlayerManager.PlayerDisconnected(other, PhotonNetwork.IsMasterClient);
        }

        private void Spawn(Soldier soldier, IReadOnlyList<IItem> equipment, Vector3 pos, Quaternion rotation)
        {
            var data = new object[equipment.Count + 1];
            data[0] = soldier.Id;
            for (var i = 0; i < equipment.Count; i++)
            {
                data[i + 1] = equipment[i].Id;
            }

            var inst = PhotonNetwork.Instantiate(soldier.Entry.prefab.name, pos, rotation, 0, data);
            PlayerManager.LocalPlayer.Controller = inst.GetComponent<SoldierController>();
            eventHandler.Send(new SpawnedEvent(PlayerManager.LocalPlayer));
            onPlayerSpawned.Invoke(PlayerManager.LocalPlayer.Controller);
            // FindObjectOfType<PlayerInventoryUI>().SetEntity(soldier);
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
        }

        private void OnPlayerConnectedToMaster(PPlayer player)
        {
            var soldier = GameManager.EntityManager.GetFirstByOwner<Soldier>(player.Id);
            var playerSpawn = FindObjectOfType<PlayerSpawn>();
            var spawnTransform = playerSpawn.GetPoint(player.Id);
            eventHandler.Send(new SpawnPlayer(soldier.Id, spawnTransform.position, spawnTransform.rotation), player.NetworkPlayer.ActorNumber);
        }
        
        private void OnSpawned(SpawnedEvent ev)
        {
            print("SPAWNED PLAYER " + ev);
            var view = PhotonNetwork.GetPhotonView(ev.ControllerViewId);
            PlayerManager.Players[(int) ev.Id].Controller = view.GetComponent<SoldierController>();
        }

        public void OnSpawn(SpawnPlayer ev)
        {
            print("READY " + ev);
            var soldier = GameManager.EntityManager.GetById<Soldier>(ev.soldierId);
            var equipment = GameManager.EntityManager.GetByOwner<Weapon>(PlayerManager.LocalPlayer.Id);
            Spawn(soldier, equipment, ev.position, ev.rotation);
        }
    }
}