﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Frontend.Controllers;
using Photon.Pun;
using Photon.Realtime;

namespace Game.Networking
{
    public class NetworkPlayerManager : IPlayerManager
    {
        public delegate void PlayerManagerEvent(PPlayer player);

        public event PlayerManagerEvent OnPlayerConnectedToMaster;
        
        public readonly List<PPlayer> Players = new List<PPlayer>(4);

        private readonly NetworkEventHandler eventHandler;

        public PPlayer LocalPlayer { get; private set; }

        public NetworkPlayerManager(NetworkEventHandler eventHandler)
        {
            this.eventHandler = eventHandler;
            eventHandler.OnSyncState += OnSyncState;

            for (var i = 0; i <= 4; i++)
            {
                Players.Add(null);
            }
        }

        public PPlayer Find(Player player)
        {
            return Players.Find(p => p != null && p.ActorNumber == player.ActorNumber);
        }

        public void Connected(Player player, bool master)
        {
            LocalPlayer = new PPlayer(player);
            if (master)
            {
                ConnectedMaster();
            }
        }

        private void ConnectedMaster()
        {
            LocalPlayer.Id = PlayerId.Player1;
            Players.Insert((byte) LocalPlayer.Id, LocalPlayer);
        }

        public void Disconnected()
        {
            LocalPlayer = null;
            Players.Clear();
        }

        public void PlayerConnected(Player other, bool master)
        {
            eventHandler.Send(new SyncStateEvent(LocalPlayer), other.ActorNumber);
            if (master)
            {
                PlayerConnectedMaster(other);
            }
        }

        private void PlayerConnectedMaster(Player other)
        {
            var index = -1;
            for (var i = 1; i <= 4; i++)
            {
                if (Players[i] != null) continue;
                index = i;
                break;
            }

            if (index == -1)
            {
                throw new Exception("ERROR: No free slot found for connected player.");
            }

            var player = new PPlayer(other) {Id = (PlayerId) index};
            Players.Insert(index, player);
            eventHandler.Send(new SyncStateEvent(player));
            OnPlayerConnectedToMaster?.Invoke(player);
        }

        public void PlayerDisconnected(Player other, bool master)
        {
            var player = Find(other);
            Players.RemoveAt((int) player.Id);
            Players.Insert((int) player.Id, null);
        }

        private void OnSyncState(SyncStateEvent ev)
        {
            Logger.Info("SYNCING PLAYER {0}", ev);

            PPlayer pp;
            if (ev.ActorNumber == LocalPlayer.NetworkPlayer.ActorNumber)
            {
                pp = LocalPlayer;
                pp.Id = ev.Id;
            }
            else
            {
                var networkPlayer = PhotonNetwork.PlayerList
                    .First(p => p.ActorNumber == ev.ActorNumber);
                pp = new PPlayer(networkPlayer) {Id = ev.Id};
            }

            Players.Insert((int) ev.Id, pp);

            if (ev.ControllerViewId != -1)
            {
                var photonView = PhotonNetwork.GetPhotonView(ev.ControllerViewId);
                var controller = photonView.GetComponent<SoldierController>();
                pp.Controller = controller;
                controller.Sync(ev.CurrentEquipment, ev.IsArmed, ev.IsAiming);
            }
        }
    }
}