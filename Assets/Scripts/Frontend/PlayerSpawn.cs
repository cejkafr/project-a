﻿using Drawing;
using Extensions;
using Unity.Mathematics;
using UnityEngine;

namespace Frontend
{
    public class PlayerSpawn : MonoBehaviourGizmos
    {
        public Transform GetPoint(PlayerId playerId)
        {
            var index = (int) playerId - 1;
            if (index < transform.childCount) 
                return transform.GetChild(index);
            Debug.LogWarning("Unable to find spawn point for player " + playerId + " returning spawner transform.");
            return transform;
        }

        public override void DrawGizmos()
        {
            var position = transform.position;
            Draw.WireBox(position.With(y: position.y + 1),
                new float3(3, 2, 3), Color.blue);
            Draw.Arrowhead(position, transform.forward, 1f, Color.blue);
            for (var i = 0; i < transform.childCount; i++)
            {
                var childPosition = transform.GetChild(i).position;
                Draw.WireCapsule(childPosition, Vector3.up, 2f, 0.5f, Color.blue);
                Draw.Label2D(childPosition, "Player " + (i + 1), Color.blue);
            }
        }
    }
}