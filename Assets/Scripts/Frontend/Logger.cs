﻿using System.Runtime.CompilerServices;
using UnityEngine;

public static class Logger
{
    public enum Type
    {
        General, Networking
    }
    
    public static void Info(string text, [CallerMemberName] string caller = "")
    {
        Debug.Log(text);
    }
    
    public static void Info(Type type, string text)
    {
        Debug.Log(type + " :: " + text);
    }
    
    public static void Info(string text, params object[] values)
    {
        Debug.Log(string.Format(text, values));
    }
    
    public static void Info(Type type, string text, params object[] values)
    {
        Debug.Log(type + " :: " + string.Format(text, values));
    }
}